package com.blazestars.bhaktisagar.CommonActivities;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.Category;
import com.blazestars.bhaktisagar.entity.NameConstant;
import com.squareup.picasso.Picasso;


public class RecyclerViewMenuAdapter extends RecyclerView.Adapter<RecyclerViewMenuAdapter.ViewHolder> {
    ArrayList<Category> data = new ArrayList<Category>();
    Context context;
    OnItemClickListener mItemClickListener;
    boolean isHomePageItems = false;
    SharedPreferences preferences;

    public RecyclerViewMenuAdapter(ArrayList<Category> data, final Context context, boolean isHomePageItems) {
        this.data = data;
        this.context = context;
        this.isHomePageItems = isHomePageItems;
        preferences = context.getSharedPreferences("languageSelected", Context.MODE_PRIVATE);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.left_menu_list_group_item
                    , parent, false);
            return new ViewHolder(view, viewType);
        }else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_home_page_item
                    , parent, false);
            return new ViewHolder(view, viewType);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (getItemViewType(position) == 0) {
            holder.itemView.setTag(data.get(position));
            if (preferences.getString("languageSelected", "").equalsIgnoreCase("hi")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.itemName.setText(Html.fromHtml(data.get(position).nameHindi,Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.itemName.setText(Html.fromHtml(data.get(position).nameHindi));
                }
            }else {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.itemName.setText(Html.fromHtml(data.get(position).name,Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.itemName.setText(Html.fromHtml(data.get(position).name));
                }
            }
        }else {
            Category category = data.get(position + 1);
            holder.itemView.setTag(category);
            if (category.type.equalsIgnoreCase(NameConstant.CATEGORY_TYPE_HOME)){
                holder.itemView.setVisibility(View.GONE);
            }else {
                holder.itemView.setVisibility(View.VISIBLE);
            }
            if (preferences.getString("languageSelected", "").equalsIgnoreCase("hi")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.itemName.setText(Html.fromHtml(category.nameHindi,Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.itemName.setText(Html.fromHtml(category.nameHindi));
                }
            }else {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.itemName.setText(Html.fromHtml(category.name,Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.itemName.setText(Html.fromHtml(category.name));
                }
            }
            if (category.type.equalsIgnoreCase(NameConstant.CATEGORY_TYPE_PANCHANG)){
                Picasso.with(context).load(R.drawable.panchang_category).into(holder.coverImage);
            }else {
                if (!"".equalsIgnoreCase(category.imagePath)) {
                    Picasso.with(context).load(category.imagePath).placeholder(R.drawable.placeholder).into(holder.coverImage);
                } else {
                    Picasso.with(context).load(R.drawable.placeholder).into(holder.coverImage);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (isHomePageItems){
            return data.size() - 3;
        }else {
            return data.size();
        }
    }

    public void insertCategories(ArrayList<Category> categories) {
        for (int i=0;i<categories.size();i++) {
            data.add(data.size(), categories.get(i));
            notifyItemInserted(data.size()-1);
        }
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (isHomePageItems){
            return 1;
        }else {
            return 0;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView itemName;
        public View itemView;
        public ImageView coverImage;

        public ViewHolder(View itemView,int viewType) {
            super(itemView);
            this.itemView = itemView;
            if (viewType == 0) {
                itemName = (TextView) itemView.findViewById(R.id.item_name);
            }else {
                itemName = (TextView) itemView.findViewById(R.id.titleName);
                coverImage = (ImageView) itemView.findViewById(R.id.coverImageView);
            }
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v,getPosition());
            }
        }
    }
}