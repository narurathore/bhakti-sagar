package com.blazestars.bhaktisagar.CommonActivities;

import android.Manifest;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.blazestars.bhaktisagar.Helper.AnalyticsHelper;
import com.blazestars.bhaktisagar.Network.MyRequestQueue;
import com.blazestars.bhaktisagar.Network.VolleyData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.NameConstant;
import com.blazestars.bhaktisagar.entity.SpacingItemDecoration;
import com.blazestars.bhaktisagar.entity.Story;
import com.blazestars.bhaktisagar.myads.MyInterstitialAd;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;


/**
 * Created by Piyush on 3/24/15.
 */
public class SearchActivity extends ActionBarActivity {

    //private final String SEARCH_URL_LAST_PART = "/0/0";
    TextView noArticleFound;
    ArrayList<Story> stories = new ArrayList<Story>();
    RecyclerView itemsRecyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    ProgressBar progressBar;
    RecyclerViewStoryAdapter storyAdapter;
    Context mContext;
    SearchView searchView;
    MenuItem searchMenuItem;
    ListView searchSuggestionListView;
    RelativeLayout rl_content;
    private ArrayList<Story> searchedSuggestedStories = new ArrayList<Story>();
    SearchNewsSuggestionAdaptor searchSuggestionAdaptor;
    RelativeLayout rl_searchSuggestion;
    boolean searchingQuery = false;
    MyInterstitialAd interstitialAd;
    int storyClickCount = 0;
    CardView searchCard;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);
        overridePendingTransition(R.anim.side_navigation_in_from_right, R.anim.side_navigation_fade_out);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("Searched Results");
        mContext = this;
        loadResources();
        searchCard = (CardView)findViewById(R.id.searchCard);
        searchSuggestionListView = (ListView)findViewById(R.id.searchSuggestion);
        rl_searchSuggestion = (RelativeLayout)findViewById(R.id.rl_searchSuggetion);
        rl_searchSuggestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_searchSuggestion.setVisibility(View.GONE);
                searchView.onActionViewCollapsed();
            }
        });
        noArticleFound = (TextView)findViewById(R.id.noArticleFound);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        rl_content = (RelativeLayout)findViewById(R.id.rl_content);
        try{

        }
        catch (Exception ex)
        {

        }
        noArticleFound.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private void setItemsRecyclerViewDecoration(){
        if (itemsRecyclerView!=null) {
            try {
                itemsRecyclerView.removeItemDecoration(SpacingItemDecoration.potraitSpacingItemDecoration);
            } catch (Exception ex) {

            }
            try {
                itemsRecyclerView.removeItemDecoration(SpacingItemDecoration.landscapeSpacingItemDecoration);
            } catch (Exception ex) {

            }
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                staggeredGridLayoutManager.setSpanCount(2);
                itemsRecyclerView.addItemDecoration(SpacingItemDecoration.landscapeSpacingItemDecoration);
            } else {
                staggeredGridLayoutManager.setSpanCount(1);
                itemsRecyclerView.addItemDecoration(SpacingItemDecoration.potraitSpacingItemDecoration);
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setItemsRecyclerViewDecoration();
    }

    private void loadResources(){
        itemsRecyclerView = (RecyclerView)findViewById(R.id.itemsRecyclerView);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        itemsRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        setItemsRecyclerViewDecoration();
        storyAdapter = new RecyclerViewStoryAdapter(stories,this);
        storyAdapter.setOnItemClickListener(new RecyclerViewStoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                openDetailPage(position);
            }
        });
        itemsRecyclerView.setAdapter(storyAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void openDetailPage(int position){
        HashMap<String,String> params = new HashMap<>();
        params.put("story_id",stories.get(position).postId);
        params.put("view_position",String.valueOf(position + 1));
        params.put("isHome","3");
        new VolleyData(this){
            @Override
            protected void VPreExecute() {

            }

            @Override
            protected void VResponse(JSONObject response, String tag) {

            }

            @Override
            protected void VError(VolleyError error, String tag) {

            }
        }.getPOSTJsonObject(NameConstant.POST_STORY_VIEWED_URL,"storyView",params);
        if (storyClickCount%3 == 0) {
            interstitialAd = new MyInterstitialAd(this, false);
        }
        storyClickCount++;
        if(!"".equalsIgnoreCase(stories.get(position).youtubeVideoId)){
            try {
                Intent intent = YouTubeStandalonePlayer.createVideoIntent(this, NameConstant.DATA_V3_GOOGLE_DEVELOPER_KEY, stories.get(position).youtubeVideoId, 0, true, false);
                startActivity(intent);
                try {
                    String categoryName = "Video Played";
                    String label = stories.get(position).title;
                    String action = stories.get(position).postId;
                    AnalyticsHelper.trackEvent(categoryName, action, label, this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }catch (Exception ex){
                Toast.makeText(this,"Unable to play video, Go to settings and report the issue",Toast.LENGTH_LONG).show();
            }
        }else {
            Intent intent = new Intent(mContext, StoryDetailActivity.class);
            intent.putExtra("position", String.valueOf(position));
            //intent.putParcelableArrayListExtra("storiesArray", stories);
            StoryDetailActivity.storiesArray = stories;
            startActivityForResult(intent, NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL);
        }
    }

    private String getSearchUrl(String searchingString){
        return NameConstant.GET_SEARCH_URL + "keyword=" + searchingString;
    }

    private String getSearchSuggestUrl(String searchingString){
        return NameConstant.GET_SEARCH_SUGGESTIONS_URL + "keyword="  + searchingString;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem menuItem = menu.findItem(R.id.search);
        searchMenuItem = menuItem;
        //searchMenuItem.setVisible(false);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    rl_searchSuggestion.setVisibility(View.GONE);
                } else {
                    rl_searchSuggestion.setVisibility(View.VISIBLE);
                    searchSuggestionListView.setVisibility(View.GONE);
                    searchCard.setVisibility(View.GONE);
                }
            }
        });
        searchSuggestionAdaptor = new SearchNewsSuggestionAdaptor(this,R.layout.search_result,searchedSuggestedStories);
        searchSuggestionListView.setAdapter(searchSuggestionAdaptor);
        searchSuggestionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Story story = (Story)v.getTag();
                searchingQuery = true;
                searchView.setQuery(story.title,true);
                openStory(story);
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                rl_searchSuggestion.setVisibility(View.GONE);
                return false;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getAndSetSearchResult(query);
                rl_searchSuggestion.setVisibility(View.GONE);
                searchingQuery = false;
                searchCard.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (searchingQuery){
                    searchingQuery=false;
                    return false;
                }
                if(newText.equalsIgnoreCase("")){
                    rl_searchSuggestion.setVisibility(View.GONE);
                    searchCard.setVisibility(View.GONE);
                    return false;
                }
                getAndSetSearchSuggestionTitles(newText);
                return false;
            }
        });

        searchView.setIconified(false);

        return true;
    }

    private void openStory(Story story){

        Intent intent = new Intent(mContext, StoryDetailActivity.class);
        intent.putExtra("notification", true);
        intent.putExtra("position", "0");
        intent.putExtra("story", story);
        //intent.putParcelableArrayListExtra("storiesArray", stories);
        startActivityForResult(intent, NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode==NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL){
                if (resultCode==RESULT_OK){
                    if (data!=null){
                        setResult(RESULT_OK,data);
                        finish();
                    }
                }
            }
        }catch (Exception ex){

        }
    }

    private void getAndSetSearchSuggestionTitles(String query){
        MyRequestQueue.Instance(this).cancelPendingRequests("search_suggestions");
        new VolleyData(this){
            @Override
            protected void VPreExecute() {

            }

            @Override
            protected void VResponse(JSONObject response, String tag) {
                try {
                    JSONArray storiesArray = response.getJSONArray("stories");
                    searchedSuggestedStories.clear();
                    for (int i = 0; i < storiesArray.length() || i<5; i++) {
                        Story story = new Story();
                        story.postId = storiesArray.getJSONObject(i).getString("id");
                        story.title = storiesArray.getJSONObject(i).getString("title");
                        searchedSuggestedStories.add(story);
                    }
                    rl_searchSuggestion.setVisibility(View.VISIBLE);
                    searchSuggestionListView.setVisibility(View.VISIBLE);
                    searchCard.setVisibility(View.VISIBLE);
                    if (searchedSuggestedStories != null) {
                        if (searchedSuggestedStories.size() == 0) {
                            rl_searchSuggestion.setVisibility(View.GONE);
                            searchCard.setVisibility(View.GONE);
                        }
                        if (searchSuggestionAdaptor != null)
                            searchSuggestionAdaptor.notifyDataSetChanged();
                    }
                } catch (Exception ex) {
                    rl_searchSuggestion.setVisibility(View.GONE);
                    searchCard.setVisibility(View.GONE);
                }
            }

            @Override
            protected void VError(VolleyError error, String tag) {
                rl_searchSuggestion.setVisibility(View.GONE);
                searchCard.setVisibility(View.GONE);
            }
        }.getJsonObject(getSearchSuggestUrl(query), "search_suggestions",this,true);


    }

    private void getAndSetSearchResult(String query){

        if(stories.size()>0) {
            stories.clear();
            storyAdapter.notifyDataSetChanged();
        }
        rl_content.setVisibility(View.VISIBLE);
        noArticleFound.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        MyRequestQueue.Instance(this).cancelPendingRequests("search");
        new VolleyData(this){
            @Override
            protected void VPreExecute() {

            }

            @Override
            protected void VResponse(JSONObject response, String tag) {
                try {
                    progressBar.setVisibility(View.INVISIBLE);
                    JSONArray storiesArray = response.getJSONArray("stories");
                    if (storiesArray.length()==0) {
                        noArticleFound.setVisibility(View.VISIBLE);
                        rl_content.setVisibility(View.GONE);
                        return;
                    }
                    noArticleFound.setVisibility(View.GONE);
                    stories.clear();
                    for (int i=0;i<storiesArray.length();i++){
                        if (i%3==0&&i!=0){
                            Story story = new Story();
                            story.isAd=true;
                            stories.add(story);
                        }
                        stories.add(new Story(mContext,storiesArray.getJSONObject(i)));
                    }
                    storyAdapter.notifyDataSetChanged();
                }catch (Exception ex){
                    noArticleFound.setVisibility(View.VISIBLE);
                    rl_content.setVisibility(View.GONE);
                }
            }

            @Override
            protected void VError(VolleyError error, String tag) {
                progressBar.setVisibility(View.INVISIBLE);
                noArticleFound.setVisibility(View.VISIBLE);
                rl_content.setVisibility(View.GONE);
            }
        }.getJsonObject(getSearchUrl(query), "search_suggestions",this,false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.side_navigation_fade_in, R.anim.side_navigation_out_to_right);
    }
}
