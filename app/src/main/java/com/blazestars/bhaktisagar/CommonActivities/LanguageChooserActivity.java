package com.blazestars.bhaktisagar.CommonActivities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.blazestars.bhaktisagar.Helper.AnalyticsHelper;
import com.blazestars.bhaktisagar.R;

import java.util.Locale;

public class LanguageChooserActivity extends ActionBarActivity {

    ImageView hindiTick;
    ImageView englishTick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_chooser);
        hindiTick = (ImageView) findViewById(R.id.tick_hindi);
        englishTick = (ImageView) findViewById(R.id.tick_english);
        SharedPreferences preferences = getSharedPreferences("languageSelected", Context.MODE_PRIVATE);
        if (getIntent().getExtras() == null || TextUtils.isEmpty(getIntent().getExtras().getString("languange_id",""))) {
            if (!TextUtils.isEmpty(preferences.getString("languageSelected", ""))) {
                String languageToLoad = preferences.getString("languageSelected", "");
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());
                SharedPreferences.Editor editor = getSharedPreferences("languageSelected", Context.MODE_PRIVATE).edit();
                editor.putString("languageSelected", languageToLoad);
                editor.commit();
                Intent intent = new Intent(this, MainHomePageActivity.class);
                startActivity(intent);
                finish();
            }
        }else {
            if (getIntent().getExtras().getString("languange_id","hi").equalsIgnoreCase("hi")){
                hindiTick.setVisibility(View.VISIBLE);
                englishTick.setVisibility(View.INVISIBLE);
            }else {
                hindiTick.setVisibility(View.INVISIBLE);
                englishTick.setVisibility(View.VISIBLE);
            }
        }
    }

    public void OnHindiSelected(View v){
        hindiTick.setVisibility(View.VISIBLE);
        englishTick.setVisibility(View.INVISIBLE);
    }

    public void OnEnglishSelected(View v){
        hindiTick.setVisibility(View.INVISIBLE);
        englishTick.setVisibility(View.VISIBLE);
    }

    public void OnContinueButtonClick(View v){
        String languageToLoad;
        if (hindiTick.getVisibility() == View.VISIBLE){
            languageToLoad = "hi";
        }else {
            languageToLoad = "en";
        }
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("languageSelected", Context.MODE_PRIVATE).edit();
        editor.putString("languageSelected", languageToLoad);
        editor.commit();
        try {
            String categoryName = "language";
            String label = "/language_id = " + languageToLoad;
            String action = "selected";
            AnalyticsHelper.trackEvent(categoryName,action,label,this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(this,MainHomePageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
