package com.blazestars.bhaktisagar.CommonActivities;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.blazestars.bhaktisagar.Network.VolleyData;
import com.blazestars.bhaktisagar.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.squareup.picasso.Picasso;


import org.json.JSONObject;

import com.blazestars.bhaktisagar.Helper.ScrollViewHelper;
import com.blazestars.bhaktisagar.entity.NameConstant;

import com.blazestars.bhaktisagar.entity.Story;

public class StoriesFragment extends Fragment{
    Story story;
    RelativeLayout contentView;
    TextView titleLabel;
    ImageView coverImage;
    int fontSize = 16;
    int storyIndex;
    LinearLayout photoGalleryView;
    LinearLayout failedLayout;
    public ScrollViewHelper scrollViewHelper;
    ColorDrawable cd;
    WebView webView;
    private ProgressDialog progressDialog;
    ProgressBar progressBar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentView = (RelativeLayout) inflater.inflate(R.layout.storyfragmentview, container, false);
        progressBar = (ProgressBar)contentView.findViewById(R.id.progressBar);
        SharedPreferences preferences = getActivity().getSharedPreferences(NameConstant.STORY_PAGE_FONT_SIZE_DATABASE_NAME,Context.MODE_PRIVATE);
        fontSize = Integer.parseInt(preferences.getString("fontSize","16"));
        photoGalleryView = (LinearLayout)contentView.findViewById(R.id.photoGalleryLayout);
        webView = (WebView) contentView.findViewById(R.id.webView);
        failedLayout = (LinearLayout)contentView.findViewById(R.id.failedLayout);
        failedLayout.setVisibility(View.INVISIBLE);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        titleLabel = (TextView)contentView.findViewById(R.id.storyTitle);
        coverImage = (ImageView)contentView.findViewById(R.id.storyThumbImage);
        titleLabel.setTextSize(fontSize+4);
        scrollViewHelper = (ScrollViewHelper)contentView.findViewById(R.id.scrollViewHelper);
        scrollViewHelper.setOnScrollViewListener(new ScrollViewHelper.OnScrollViewListener() {
            @Override
            public void onScrollChanged(ScrollViewHelper v, int l, int t, int oldl, int oldt) {
                //setTitleAlpha(255 - getAlphaforActionBar(v.getScrollY()));
                cd.setAlpha(getAlphaforActionBar(v.getScrollY()));
            }

        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVisibility(View.GONE);
        final ProgressBar progressBarWebView = (ProgressBar) contentView.findViewById(R.id.progress_bar_web_view);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view, String url) {
                progressBarWebView.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.VISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            }


        });

        if (story == null || story.body.equalsIgnoreCase("")) {
            loadStoryFromURL();
        }else {
            setData();
        }
        AdView adView = (AdView) contentView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        return contentView;
    }

    private void setData(){
        if (!story.fullImage.equalsIgnoreCase(""))
            Picasso.with(getActivity()).load(story.fullImage).placeholder(R.drawable.placeholder).into(coverImage);
        //titleLabel.setText(story.title);
        SharedPreferences preferences = getActivity().getSharedPreferences("languageSelected", Context.MODE_PRIVATE);
        if (preferences.getString("languageSelected", "").equalsIgnoreCase("hi")) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                titleLabel.setText(Html.fromHtml(story.titleHindi,Html.FROM_HTML_MODE_LEGACY));
            } else {
                titleLabel.setText(Html.fromHtml(story.titleHindi));
            }
        }else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                titleLabel.setText(Html.fromHtml(story.title,Html.FROM_HTML_MODE_LEGACY));
            } else {
                titleLabel.setText(Html.fromHtml(story.title));
            }
        }
        webView.loadDataWithBaseURL("file:///android_asset/", getHtmlStringData(story.body), "text/html", "UTF-8", null);
        scrollViewHelper.setVisibility(View.VISIBLE);
        failedLayout.setVisibility(View.INVISIBLE);
    }
    private void loadStoryFromURL(){
        String url;
        try {
            url = getStoryURL();
            progressBar.setVisibility(View.VISIBLE);
            scrollViewHelper.setVisibility(View.GONE);
            new VolleyData(getActivity()){
                @Override
                protected void VPreExecute() {

                }

                @Override
                protected void VResponse(JSONObject response, String tag) {
                    progressBar.setVisibility(View.GONE);
                    JSONObject json = response;
                    try {
                        if (json!=null){
                            JSONObject storyJSON = json.getJSONObject("story");
                            story = new Story(getActivity(),storyJSON);
                            story.storyJSONString = storyJSON.toString();
                            setData();
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                        if (story.body.equalsIgnoreCase(""))
                            failedLayout.setVisibility(View.VISIBLE);
                        else
                            scrollViewHelper.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                protected void VError(VolleyError error, String tag) {
                    progressBar.setVisibility(View.INVISIBLE);
                    if (story == null || story.body.equalsIgnoreCase(""))
                        failedLayout.setVisibility(View.VISIBLE);
                    else
                        scrollViewHelper.setVisibility(View.VISIBLE);
                }
            }.getJsonObject(url,"story",getActivity(),true);
        }
        catch (Exception Ex)
        {
            progressBar.setVisibility(View.GONE);
            if (story == null || story.body.equalsIgnoreCase(""))
                failedLayout.setVisibility(View.VISIBLE);
            else
                scrollViewHelper.setVisibility(View.VISIBLE);
        }
    }

    public void loadVideoIfExist(){
        if (!story.youtubeVideoId.equalsIgnoreCase("")) {
            Intent intent = YouTubeStandalonePlayer.createVideoIntent(getActivity(), NameConstant.DATA_V3_GOOGLE_DEVELOPER_KEY, story.youtubeVideoId, 0, true, false);
            startActivity(intent);
        }
    }

    private String getStoryURL(){

            return NameConstant.GET_STORY_URL + "id=" + story.postId;
    }


    public static StoriesFragment newInstance(Story story, int index, ColorDrawable cd) {

        StoriesFragment f = new StoriesFragment();
        f.story = story;
        f.storyIndex = index;
        f.cd = cd;
        if (f.story==null)
            f.story = new Story();
        return f;
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        public boolean onDown(MotionEvent e) {
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            System.out.println("single Tap");
        //    ((StoryDetailActivity) getActivity()).toggleTopBar();
            // ImageViewerActivity(getActivity())
            return true;
        }

    }

    public int getAlphaforActionBar(int scrollY) {
        int minDist = 0, maxDist = 550;
        if (scrollY > maxDist) {
            return 255;
        } else {
            if (scrollY < minDist) {
                return 0;
            } else {
                return (int) ((255.0 / maxDist) * scrollY);
            }
        }
    }

    private String getHtmlStringData(String body)
    {
        String htmlStr = "<style type=text/css>" +
                "@font-face {\n" +
                "    font-family: 'Desc-Font';\n" +
                "    src: url('fonts/descfont.ttf');\n" +
                "}"+
                "#multicolumn{column-count:1;column-gap: 5px;column-rule:1px solid#E8E8E8;\n" +
                "line-height:1.4;" +
                "margin:0 0;}\n" +
                " body {} " +
                "a {color: #0000cc;}"+
                "h{color:#AC182E; font-family:'Desc-Font'; line-height:1.2;}\n" +
                "img {vertical-align:center !important;display:block !important; horizontal-align:center !important;margin-bottom:5 !important; min-height:50 !important;height:auto !important; margin-left: 0 !important;   margin-right: 0 !important; max-width:100% !important; border:none !important; }\n" +
                "iframe {vertical-align:center !important;display:block !important; horizontal-align:center !important;margin-bottom:5 !important; min-height:50 !important; margin-left: 0 !important;   margin-right: 0 !important; max-width:100% !important; border:none !important; }\n" +
                "div {max-width:100%;height:auto;}"+
                "</style>" +
                "<html><head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'></head><body>" +
                "<div style='margin:0 5;' >";
        htmlStr = htmlStr+"<div id=multicolumn style='color:#4f4f4f;font-size:"+fontSize+" !important; font-family:'Desc-Font' line-height:1.5;'>"+body+"</div>" + "</div></body></html>" +
                "<script type='text/javascript' src='https://platform.twitter.com/widgets.js'></script>" +
                "<script type='text/javascript' src='http://platform.instagram.com/en_US/embeds.js'></script>";
        return htmlStr;
    }
}