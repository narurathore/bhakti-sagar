package com.blazestars.bhaktisagar.CommonActivities;

import android.content.Context;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.blazestars.bhaktisagar.Helper.Helper;
import com.blazestars.bhaktisagar.R;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.blazestars.bhaktisagar.entity.Story;


public class RecyclerViewStoryAdapter extends RecyclerView.Adapter<RecyclerViewStoryAdapter.ViewHolder> {
    ArrayList<Story> data = new ArrayList<Story>();
    private boolean isLoadingMoreData =false ;
    Context context;
    View.OnClickListener titleListener;


    OnItemClickListener mItemClickListener;
    OnItemClickListener mOnlongClickListner;
    SharedPreferences preferences;

    RequestQueue requestQueue;
    public RecyclerViewStoryAdapter(ArrayList<Story> data,final Context context) {
        this.data = data;
        this.context = context;
        preferences = context.getSharedPreferences("languageSelected", Context.MODE_PRIVATE);
        requestQueue = Volley.newRequestQueue(context);

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType==0) {
            //For Article with Image
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.others_row_grid
                    , parent, false);
        }else {
            //For Loading
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_bottom_progressbar
                    , parent, false);
        }
        return new ViewHolder(view,viewType);
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setOnItemLongClickListner(final OnItemClickListener mItemLongClickListener) {
        this.mOnlongClickListner = mItemLongClickListener;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        final Story item;
        if (viewType==0) {

            //For Article
            item = data.get(position);
            holder.position = position;
            if(!"".equalsIgnoreCase(item.thumbImage)) {
                Picasso.with(context).load(item.thumbImage).placeholder(R.drawable.placeholder).into(holder.coverImage);
            }else {
                Picasso.with(context).load(R.drawable.placeholder).placeholder(R.drawable.placeholder).into(holder.coverImage);
            }
            if (preferences.getString("languageSelected", "").equalsIgnoreCase("hi")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.titleName.setText(Html.fromHtml(item.titleHindi,Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.titleName.setText(Html.fromHtml(item.titleHindi));
                }

            }else {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    holder.titleName.setText(Html.fromHtml(item.title,Html.FROM_HTML_MODE_LEGACY));
                } else {
                    holder.titleName.setText(Html.fromHtml(item.title));
                }

            }
            if ("".equalsIgnoreCase(item.youtubeVideoId)){
                holder.playImage.setVisibility(View.GONE);
            }else {
                holder.playImage.setVisibility(View.VISIBLE);
            }
            if (new FeedWLDBHelper(context).checkIfBookmarked(item.postId)){
                Drawable starFull = new IconDrawable(context, Iconify.IconValue.fa_star )
                        .colorRes(R.color.Black)
                        .actionBarSize();
                holder.bookmarkImage.setImageDrawable(starFull);
            }else {
                Drawable starEmpty = new IconDrawable(context, Iconify.IconValue.fa_star_o )
                        .colorRes(R.color.Black)
                        .actionBarSize();
                holder.bookmarkImage.setImageDrawable(starEmpty);
            }
            holder.bookmark_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FeedWLDBHelper dbHelper = new FeedWLDBHelper(context);
                    if (dbHelper.checkIfBookmarked(item.postId)) {
                        dbHelper.removeFromBookmarked(item.postId);
                        Drawable starEmpty = new IconDrawable(context, Iconify.IconValue.fa_star_o )
                                .colorRes(R.color.Black)
                                .actionBarSize();
                        holder.bookmarkImage.setImageDrawable(starEmpty);
                        Toast.makeText(context,"Story removed from My Bookmarks.",Toast.LENGTH_SHORT).show();
                    } else {
                        dbHelper.addToBookmarked(item);
                        Drawable starFull = new IconDrawable(context, Iconify.IconValue.fa_star )
                                .colorRes(R.color.Black)
                                .actionBarSize();
                        holder.bookmarkImage.setImageDrawable(starFull);
                        Toast.makeText(context,"Story added to My BookMarks.",Toast.LENGTH_SHORT).show();
                    }
                }
            });
            holder.share_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Helper.openShareDialog(item,context);

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (isLoadingMoreData)
             return data.size()+1;
        else
            return data.size();
    }

    public void insertStories(ArrayList<Story> stories) {
        for (int i=0;i<stories.size();i++) {
            data.add(data.size(), stories.get(i));
            notifyItemInserted(data.size());
        }
    }

    public void insertMoreLoading(){
        isLoadingMoreData = true;
        try {
            notifyItemInserted(data.size());
        }catch (Exception ex){

        }

    }

    public void removeMoreLoading(){
        if (isLoadingMoreData) {
            isLoadingMoreData = false;
            try {
                notifyItemRemoved(data.size());
            }catch (Exception ex){

            }

        }
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position==data.size()) {
            return -1;
        }else{
            return 0;
        }
    }



    //
//    private int getRandomColor() {
//        SecureRandom rgen = new SecureRandom();
//        return Color.HSVToColor(150, new float[]{
//                rgen.nextInt(359), 1, 1
//        });
//    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView titleName;
        ImageView coverImage;
        ImageView playImage;
        ProgressBar progressBar;
        int position;
        LinearLayout bookmark_ll;
        LinearLayout share_ll;
        ImageView bookmarkImage;
        public ViewHolder(View itemView,int viewType) {
            super(itemView);
            if (viewType==0) {
                titleName = (TextView) itemView.findViewById(R.id.titleName);
                coverImage = (ImageView) itemView.findViewById(R.id.coverImageView);
                playImage = (ImageView) itemView.findViewById(R.id.play_image);
                bookmark_ll = (LinearLayout) itemView.findViewById(R.id.bookmark_ll);
                share_ll = (LinearLayout) itemView.findViewById(R.id.share_ll);
                progressBar = (ProgressBar)itemView.findViewById(R.id.progress_bar);
                bookmarkImage = (ImageView)itemView.findViewById(R.id.bookmark_image);
                itemView.setOnClickListener(this);
                if (mOnlongClickListner!=null)
                    itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            if (mOnlongClickListner!=null)
                                mOnlongClickListner.onItemClick(v,getPosition());
                            return false;
                        }
                    });
            }
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v,getPosition());
            }
        }
    }
}