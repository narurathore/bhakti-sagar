package com.blazestars.bhaktisagar.CommonActivities;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.CardDailyPanchangModel;

import java.util.List;

/**
 * Created by narayan on 10/6/15.
 */
public class CardsPanchangAdaptor extends RecyclerView.Adapter<CardsPanchangAdaptor.ViewHolder> {
    private List<CardDailyPanchangModel> cards;
    RecyclerViewMenuAdapter.OnItemClickListener mItemClickListener;

    public CardsPanchangAdaptor(List<CardDailyPanchangModel> cards) {
        this.cards = cards;
    }

    @Override
    public CardsPanchangAdaptor.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_panchang
                , parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CardsPanchangAdaptor.ViewHolder holder, int position) {
        loadCardDataInWebView(cards.get(position), holder.webView);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        WebView webView;
        public ViewHolder(View itemView) {
            super(itemView);
            webView = (WebView) itemView.findViewById(R.id.cardWebView);
        }
    }

    private void loadCardDataInWebView(CardDailyPanchangModel card, WebView webView){
        String webstr = "<html><head>" + card.style
                + "<style>@font-face {font-family:'opensans';src: url" +
                "('file:///android_asset/fonts/regular.ttf';font-weight: normal);}body " +
                "{font-family:'opensans';}img{PADDING-BOTTOM: 5px;max-width:100%;}</style><script>window.onload = " +
                "callOnload;window.onscroll=callOnScroll;function showLoader(){document.getElementById('loader')." +
                "style.display='inline'};function hideLoader(){document.getElementById('loader').style.display='none'};" +
                "function toggleFont(){var d=document.getElementById('multicolumn');var d1=document.getElementById" +
                "('headline');if(d.className == 'large'){d.className='';}else{d.className='large';}if(d1.className == 'large')" +
                "{d1.className='title';}else{d1.className='large';}};function callOnload(){ hideLoader();}</script></head><body >";
        webstr += card.cardHTML;
        webstr += "</body></html>";
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL("", webstr, "text/html", "UTF-8", "");
        webView.setBackgroundColor(Color.WHITE);
    }
}
