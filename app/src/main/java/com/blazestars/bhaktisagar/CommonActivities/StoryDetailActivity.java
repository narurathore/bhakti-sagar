package com.blazestars.bhaktisagar.CommonActivities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.util.LruCache;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.blazestars.bhaktisagar.Helper.Helper;
import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.myads.MyInterstitialAd;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;
import org.json.JSONObject;
import java.util.ArrayList;
import com.blazestars.bhaktisagar.Helper.AnalyticsHelper;
import com.blazestars.bhaktisagar.entity.Category;
import com.blazestars.bhaktisagar.entity.NameConstant;
import com.blazestars.bhaktisagar.entity.Story;


public class StoryDetailActivity extends ActionBarActivity{
    private StoryDetailPageAdapter pagerAdapter;
    ArrayList<Story> stories = new ArrayList<Story>();
    public static ArrayList<Story> storiesArray = new ArrayList<Story>();
    int position;
    FeedWLDBHelper dbHelper = new FeedWLDBHelper(this);
    static public Context storyDetailContext;
    Boolean isCurrentPostBookmarked;
    ViewPager pager;
    MenuItem bookmarkButton;
    int selectedFontItemNumber = 0;
    TextView toolBarTitle;
    MyInterstitialAd interstitialAd;
    int pageSwipeCount = 1;
    ColorDrawable cd;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story_detail);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        cd  = new ColorDrawable(getResources().getColor(R.color.primaryColor));
        getSupportActionBar().setBackgroundDrawable(cd);
        cd.setAlpha(0);
        stories.clear();
        try {
            String categoryName = "Story";
            String label = "/Story Detail Page opened";
            String action = "Opened";
            AnalyticsHelper.trackEvent(categoryName, action, label, StoryDetailActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String positionStr = getIntent().getExtras().getString("position","0");
        try {
            position = Integer.parseInt(positionStr);
        } catch (Exception e) {
            position = 0;
        }
        if (getIntent().getExtras().getBoolean("notification",false)){
                Story story = (Story) getIntent().getExtras().get("story");
                stories.clear();
                if (story != null) {
                    stories.add(story);
                } else {
                    onBackPressed();
                    return;
                }
        }else {

            try {
                for (int i=0;i<storiesArray.size();i++){
                    stories.add(new Story(this,new JSONObject(storiesArray.get(i).storyJSONString)));
                }
            }catch (Exception ex){

            }
        }
        if (stories.size()==0){
            return;
        }
        toolBarTitle = (TextView)findViewById(R.id.toolbar_title);
        storyDetailContext = this;
        setFont();
        try {
            //stories = getIntent().getParcelableArrayListExtra("storiesArray");
            toolBarTitle.setText((String.valueOf(position + 1)) + "/" + String.valueOf(stories.size()));
        } catch (Exception gridArrayEx) {

        }

        setads();
    }

    private void setads() {
        /*LinearLayout layout = (LinearLayout)(findViewById(R.id.adrelativeLayout)).findViewById(R.id.linearLayout);
        layout.removeAllViews();
        Helper.setAd(layout, this, false);*/
    }

    private void setBookmarkStar(boolean isStarEmpty){
        if(isStarEmpty)
        {
            Drawable starEmptyLight = new IconDrawable(this, Iconify.IconValue.fa_star_o )
                    .colorRes(R.color.white)
                    .actionBarSize();
            bookmarkButton.setIcon(starEmptyLight);
        }else{
            Drawable starFullLight = new IconDrawable(this, Iconify.IconValue.fa_star )
                    .colorRes(R.color.white)
                    .actionBarSize();
            bookmarkButton.setIcon(starFullLight);
        }
    }

    private void setFont(){
        SharedPreferences preferences = getSharedPreferences(NameConstant.STORY_PAGE_FONT_SIZE_DATABASE_NAME,MODE_PRIVATE);
        String fontSize = preferences.getString("fontSize","14");
        selectedFontItemNumber = (Integer.parseInt(fontSize)-10)/2;
    }

    public void setPager(){
        pager = (ViewPager) findViewById(R.id.storydetailviewpager);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                try {

/*                pageHeaderLabel.setText(String.valueOf(arg0+1)+"/"+String.valueOf(gridArray.size()));*/
                    position = arg0;
                    ((StoriesFragment)pagerAdapter.getItem(position)).loadVideoIfExist();
                    toolBarTitle.setText((String.valueOf(position + 1)) + "/" + String.valueOf(stories.size()));
                    updateScreen();
                    StoriesFragment storiesFragment = (StoriesFragment) pagerAdapter.getItem(arg0);
                    if (cd != null) {
                        if (storiesFragment.scrollViewHelper != null)
                            cd.setAlpha(storiesFragment.getAlphaforActionBar(storiesFragment.scrollViewHelper.getScrollY()));
                    }
                    try {
                        String categoryName = "Story";
                        String label = "/Story Read";
                        String action = "read";
                        AnalyticsHelper.trackEvent(categoryName, action, label, StoryDetailActivity.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        String pageviews = "/Story opened with id " + stories.get(position).postId + "/title = " + stories.get(position).title;
                        AnalyticsHelper.trackPageView(pageviews, StoryDetailActivity.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    pageSwipeCount++;
                    if (pageSwipeCount > 0 && pageSwipeCount % 2 == 0) {
                        interstitialAd = new MyInterstitialAd(storyDetailContext, false);
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
        pagerAdapter = new StoryDetailPageAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(position);
        ((StoriesFragment)pagerAdapter.getItem(pager.getCurrentItem())).loadVideoIfExist();
        try {
            String categoryName = "Story";
            String label = "/Story Read" ;
            String action = "read";
            AnalyticsHelper.trackEvent(categoryName, action, label, StoryDetailActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        updateScreen();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode==NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL){
                if (resultCode==RESULT_OK){
                    if (data!=null){
                        setResult(RESULT_OK,data);
                        finish();
                    }
                }
            }
        }catch (Exception ex)
        {

        }
    }

    public void openCategory(Category category){
        Intent data = new Intent();
        data.putExtra("category",category);
        setResult(RESULT_OK,data);
        finish();
    }

    public void updateScreen()
    {
        checkBookmarked(position);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(this.isTaskRoot()){
            Intent home = new Intent(this,MainHomePageActivity.class);
            startActivity(home);
            finish();
            return;
        }
        setResult(RESULT_OK);
        finish();
    }

    public void fontButtonTapped()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final String[] items = new String[10];
        for (int i=0;i<items.length;i++){
            items[i] = String.valueOf(10+i*2);
        }
        builder.setTitle("Font Size");
        builder.setSingleChoiceItems(items, selectedFontItemNumber, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor editor = getSharedPreferences(NameConstant.STORY_PAGE_FONT_SIZE_DATABASE_NAME,MODE_PRIVATE).edit();
                editor.putString("fontSize", items[which]);
                selectedFontItemNumber = which;
                editor.apply();
                dialog.cancel();
                setPager();
                pager.setCurrentItem(position);
            }
        });
        builder.show();
    }

    public void checkBookmarked(int position) {
        try {
            if (bookmarkButton == null)
                return;
            Story story = stories.get(position);
            isCurrentPostBookmarked = dbHelper.checkIfBookmarked(story.postId);
            if (isCurrentPostBookmarked) {
                setBookmarkStar(false);
            } else {
                setBookmarkStar(true);
            }
        } catch (Exception bookmarkException) {

        }
    }

    private class StoryDetailPageAdapter extends FragmentStatePagerAdapter {
        private class MyCache extends LruCache<Integer, Fragment> {

            public MyCache(int maxSize) {
                super(maxSize);
            }

            @Override
            protected Fragment create(Integer key) {
                Story story = stories.get(key);
                return StoriesFragment.newInstance(story, key, cd);
            }
        }

        private final MyCache mCache;

        public StoryDetailPageAdapter(FragmentManager fm) {
            super(fm);

            mCache = new MyCache(3);

        }

        @Override
        public Fragment getItem(int pos) {
            return mCache.get(pos);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            mCache.remove(position);
            super.destroyItem(container, position, object);
        }

        @Override
        public int getCount() {
            return stories.size();
        }
    }

    public void favoriteStoryButtonTapped()
    {
        toggleFavorite();
    }

    private void toggleFavorite(){
        try {
            int index = pager.getCurrentItem();
            Story story = stories.get(index);
            if(story.storyJSONString!=null && !story.storyJSONString.equalsIgnoreCase("")) {
                if (dbHelper.checkIfBookmarked(story.postId)) {
                    dbHelper.removeFromBookmarked(story.postId);
                    setBookmarkStar(true);
                    Toast.makeText(this,"Story removed from My Bookmarks.",Toast.LENGTH_SHORT).show();
                } else {
                    dbHelper.addToBookmarked(story);
                    setBookmarkStar(false);
                    Toast.makeText(this,"Story added to My BookMarks.",Toast.LENGTH_SHORT).show();
                    try {
                        String categoryName = "Story";
                        String label = "/Story added to My Bookmarks" ;
                        String action = "bookmarked";
                        AnalyticsHelper.trackEvent(categoryName, action, label, StoryDetailActivity.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }catch (Exception ex)
        {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        final float scale = getResources().getDisplayMetrics().density;
        int heightinDp = (int)(displayMetrics.heightPixels/scale);
        int heightPixels = displayMetrics.heightPixels;
        Log.d("Heightinpixels : ",String.valueOf(heightPixels));
        Log.d("Heightindp : ", String.valueOf(heightinDp));
        getMenuInflater().inflate(R.menu.menu_story_detail_activity, menu);
        Drawable fontIconLight = new IconDrawable(this, Iconify.IconValue.fa_font )
                .colorRes(R.color.white)
                .actionBarSize();
        Drawable shareIconDark = new IconDrawable(this, Iconify.IconValue.fa_share )
                .colorRes(R.color.white)
                .actionBarSize();
        menu.findItem(R.id.action_font).setIcon(fontIconLight);
        //menu.findItem(R.id.action_share).setIcon(shareIconDark);
        bookmarkButton = menu.findItem(R.id.action_favorite);
        setBookmarkStar(true);
        if(stories.size()>0)
            setPager();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        switch (id){
            case R.id.action_font:
                fontButtonTapped();
                return true;
            case R.id.action_favorite:
                favoriteStoryButtonTapped();
                return true;
            case R.id.action_share:
                if (stories != null && pager != null && stories.size() > pager.getCurrentItem())
                Helper.openShareDialog(stories.get(pager.getCurrentItem()),this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
