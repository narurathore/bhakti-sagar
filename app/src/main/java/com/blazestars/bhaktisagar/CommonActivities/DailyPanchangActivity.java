package com.blazestars.bhaktisagar.CommonActivities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.blazestars.bhaktisagar.Helper.AnalyticsHelper;
import com.blazestars.bhaktisagar.Network.MyRequestQueue;
import com.blazestars.bhaktisagar.Network.VolleyData;
import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.TextViews.TitleTextView;
import com.blazestars.bhaktisagar.entity.CardDailyPanchangModel;
import com.blazestars.bhaktisagar.entity.Category;
import com.blazestars.bhaktisagar.entity.DailyPanchangModel;
import com.blazestars.bhaktisagar.entity.NameConstant;
import com.blazestars.bhaktisagar.entity.SearchedCityPanchang;
import com.blazestars.bhaktisagar.myads.MyInterstitialAd;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DailyPanchangActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    TextView dateET;
    EditText placeET;
    RecyclerView searchCityRecyclerView;
    //RecyclerView cardsRecyclerView;
    SearchCitiesPanchangAdaptor citiesPanchangAdaptor;
    LinearLayout searchListLayout;
    ProgressBar cityProgressBar;
    ProgressBar panchangProgressBar;
    LinearLayout cardLayout;
    TextView titleTV;
    SearchedCityPanchang selectedCity;
    SharedPreferences preferences;
    String selectedLanguage;
    String lidFromPrefences;
    String cityTitleFromPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panchang);
        try {
            String categoryName = "Panchang";
            String label = "/Daily Panchang";
            String action = "Opened";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        preferences = getSharedPreferences("languageSelected", MODE_PRIVATE);
        selectedLanguage = preferences.getString("languageSelected", "hi");
        preferences = getSharedPreferences("cityInfo",MODE_PRIVATE);
        lidFromPrefences = preferences.getString("lid","-1");
        cityTitleFromPreferences = preferences.getString("cityTitle","");
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.panchang));
        dateET = (TextView) findViewById(R.id.date);
        placeET = (EditText) findViewById(R.id.city);
        placeET.setText(cityTitleFromPreferences);
        if (!lidFromPrefences.equalsIgnoreCase("")){
            SearchedCityPanchang cityPanchang = new SearchedCityPanchang();
            cityPanchang.id = lidFromPrefences;
            cityPanchang.place = cityTitleFromPreferences;
            selectedCity = cityPanchang;
        }
        searchCityRecyclerView = (RecyclerView)findViewById(R.id.place_search_recycler_view);
        searchCityRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        cardLayout = (LinearLayout) findViewById(R.id.cardLayout);
        //cardsRecyclerView = (RecyclerView) findViewById(R.id.cardRecyclerView);
        //cardsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        searchListLayout = (LinearLayout) findViewById(R.id.searchListLayout);
        cityProgressBar = (ProgressBar) findViewById(R.id.city_progress_bar);
        panchangProgressBar = (ProgressBar) findViewById(R.id.panchang_progress_bar);
        titleTV = (TextView) findViewById(R.id.storyTitle);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        dateET.setText(formattedDate.trim());
        placeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().contains(",")) {
                    if (charSequence.length() > 2) {
                        selectedCity = null;
                        callPlaceSearchAPI(charSequence.toString());
                    } else {
                        selectedCity = null;
                        searchListLayout.setVisibility(View.GONE);
                    }
                }else {
                    searchListLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        citiesPanchangAdaptor = new SearchCitiesPanchangAdaptor(new ArrayList<SearchedCityPanchang>());
        citiesPanchangAdaptor.setOnItemClickListener(new RecyclerViewMenuAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                hideKeyboard();
                SearchedCityPanchang cityPanchang = (SearchedCityPanchang) view.getTag();
                String title = "";
                if (!TextUtils.isEmpty(cityPanchang.place)){
                    title = cityPanchang.place;
                }
                if (!TextUtils.isEmpty(cityPanchang.state)){
                    if (!TextUtils.isEmpty(title)){
                        title += ", ";
                    }
                    title += cityPanchang.state;
                }
                if (!TextUtils.isEmpty(cityPanchang.country)){
                    if (!TextUtils.isEmpty(title)){
                        title += ", ";
                    }
                    title += cityPanchang.country;
                }
                placeET.setText(title);
                selectedCity = cityPanchang;
                SharedPreferences.Editor editor = getSharedPreferences("cityInfo", Context.MODE_PRIVATE).edit();
                editor.putString("cityTitle", title);
                editor.putString("lid",cityPanchang.id);
                editor.commit();
                loadPanchang(cityPanchang.id.toString(),dateET.getText().toString());
            }
        });
        searchCityRecyclerView.setAdapter(citiesPanchangAdaptor);
        loadPanchang(lidFromPrefences,dateET.getText().toString());
        //new Handler().postDelayed(new showAds(), 5000);
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about_us, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(this.isTaskRoot()){
            Intent home = new Intent(this,MainHomePageActivity.class);
            startActivity(home);
            finish();
            return;
        }
        super.onBackPressed();
    }

    public void openDatePicker(View v){
// Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        new DatePickerDialog(this, this, year, month, day).show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        dateET.setText(datePicker.getDayOfMonth() + "-" + (datePicker.getMonth() + 1) + "-" + datePicker.getYear());
    }

    private void callPlaceSearchAPI(String searchPlace){
        MyRequestQueue.Instance(this).cancelPendingRequests(searchPlace);
        new VolleyData(this){
            @Override
            protected void VPreExecute() {
                cityProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected void VResponse(JSONObject response, String tag) {
                if (tag.equalsIgnoreCase(placeET.getText().toString())) {
                    cityProgressBar.setVisibility(View.GONE);
                    JSONObject json = response;
                    try {
                        if (json != null) {
                            JSONArray jsonCities = json.getJSONArray("cities");
                            citiesPanchangAdaptor.clearData();
                            List<SearchedCityPanchang> searchedCityPanchangList = new ArrayList<SearchedCityPanchang>();
                            for (int i = 0; i < jsonCities.length(); i++) {
                                searchedCityPanchangList.add(new SearchedCityPanchang(jsonCities.getJSONObject(i)));
                            }
                            citiesPanchangAdaptor.insertData(searchedCityPanchangList);
                            searchListLayout.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        searchListLayout.setVisibility(View.GONE);
                        showAlertWithWriteToUSOption("",getString(R.string.panchang_unknown_error));
                    }
                }else {
                    if (placeET.getText().length() < 3){
                        cityProgressBar.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            protected void VError(VolleyError error, String tag) {
                cityProgressBar.setVisibility(View.GONE);
                searchListLayout.setVisibility(View.GONE);
                showAlert("",getString(R.string.panchang_please_check_internet));
            }
        }.getJsonObject(NameConstant.PLACE_URL_PANCHANG + "?placename=" + searchPlace.replace(" ","%20"), searchPlace,this,false);
    }

    private void loadPanchang(String lid,String date){
        searchListLayout.setVisibility(View.GONE);
        MyRequestQueue.Instance(this).cancelPendingRequests("panchang");
        String url = NameConstant.URL_PANCHANG + "?date=" + date + "&language=" + selectedLanguage + "&lid=" + lid;
        new VolleyData(this){
            @Override
            protected void VPreExecute() {
                //cardsRecyclerView.setVisibility(View.GONE);
                cardLayout.setVisibility(View.GONE);
                titleTV.setVisibility(View.GONE);
                panchangProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected void VResponse(JSONObject response, String tag) {
                JSONObject json = response;
                try {
                    if (json != null) {
                        showPanchang(new DailyPanchangModel(json));
                    }else {
                        showAlertWithWriteToUSOption("",getString(R.string.panchang_unknown_error));
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                    searchListLayout.setVisibility(View.GONE);
                    panchangProgressBar.setVisibility(View.GONE);
                    showAlertWithWriteToUSOption("",getString(R.string.panchang_unknown_error));
                }
            }

            @Override
            protected void VError(VolleyError error, String tag) {
                searchListLayout.setVisibility(View.GONE);
                panchangProgressBar.setVisibility(View.GONE);
                showAlert("",getString(R.string.panchang_please_check_internet));
            }
        }.getJsonObject(url, "panchang",this,false);
    }

    private void showPanchang(DailyPanchangModel dailyPanchang){
        //new MyInterstitialAd(DailyPanchangActivity.this, false);
        try {
            String categoryName = "Panchang";
            String label = "/Daily Panchang";
            String action = "Loaded And Shown";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        searchListLayout.setVisibility(View.GONE);
        titleTV.setVisibility(View.VISIBLE);
        cardLayout.setVisibility(View.VISIBLE);
        //cardsRecyclerView.setVisibility(View.VISIBLE);
        cardLayout.removeAllViews();
        for (int i = 0; i < dailyPanchang.cards.size(); i++){
            View view = LayoutInflater.from(this).inflate(R.layout.card_panchang
                    , cardLayout, false);
            WebView webView = (WebView) view.findViewById(R.id.cardWebView);
            loadCardDataInWebView(dailyPanchang.cards.get(i),webView);
            cardLayout.addView(view);
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            titleTV.setText(Html.fromHtml(dailyPanchang.title,Html.FROM_HTML_MODE_LEGACY));
        } else {
            titleTV.setText(Html.fromHtml(dailyPanchang.title));
        }
        panchangProgressBar.setVisibility(View.GONE);
        //CardsPanchangAdaptor cardsPanchangAdaptor = new CardsPanchangAdaptor(dailyPanchang.cards);
        //cardsRecyclerView.setAdapter(cardsPanchangAdaptor);
    }

    private void loadCardDataInWebView(CardDailyPanchangModel card, WebView webView){
        String webstr = "<html><head>" + card.style
                + "<style>@font-face {font-family:'opensans';src: url" +
                "('file:///android_asset/fonts/regular.ttf';font-weight: normal);}body " +
                "{font-family:'opensans';}img{PADDING-BOTTOM: 5px;max-width:100%;}</style><script>window.onload = " +
                "callOnload;window.onscroll=callOnScroll;function showLoader(){document.getElementById('loader')." +
                "style.display='inline'};function hideLoader(){document.getElementById('loader').style.display='none'};" +
                "function toggleFont(){var d=document.getElementById('multicolumn');var d1=document.getElementById" +
                "('headline');if(d.className == 'large'){d.className='';}else{d.className='large';}if(d1.className == 'large')" +
                "{d1.className='title';}else{d1.className='large';}};function callOnload(){ hideLoader();}</script></head><body >";
        webstr += card.cardHTML;
        webstr += "</body></html>";
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL("", webstr, "text/html", "UTF-8", "");
        webView.setBackgroundColor(Color.WHITE);
    }

    public void onViewPanchangClick(View v){
        if (selectedCity != null) {
            loadPanchang(selectedCity.id, dateET.getText().toString());
        }else {
            showAlert("",getString(R.string.panchang_please_select_city));
        }
    }

    private void showAlertWithWriteToUSOption(String title, String message){
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(getString(R.string.panchang_write_us), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent Email = new Intent(Intent.ACTION_SEND);
                        Email.setType("text/email");
                        Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"allbhaktiserials@gmail.com"});
                        Email.putExtra(Intent.EXTRA_SUBJECT, "Feedback For "+ getString(R.string.app_name)+" android application");
                        //AnalyticsHelper.MobileInfo mobileInfo = new AnalyticsHelper.MobileInfo(context);
                        //Email.putExtra(Intent.EXTRA_TEXT, "\tDevice Info :\n\tModel Name : " + mobileInfo.getMobileName() + "\n\tAndroid OS Version : " + mobileInfo.getMobileOs()+ "\n\tMobile Resolution : " + mobileInfo.getMobileResolution()+ "\n\tManufacturer : " + mobileInfo.getManufacturer()+ "\n\tApplication Version : " + mobileInfo.getappVersionName()+"\n\n");
                        startActivity(Intent.createChooser(Email, "Send Feedback:"));
                    }
                })
                .setCancelable(false)
                .setNegativeButton(getString(R.string.panchang_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    private void showAlert(String title, String message){
        try {
            new AlertDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(getString(R.string.panchang_okay), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .setCancelable(true)
                    .show();
        }catch (Exception ex){

        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
