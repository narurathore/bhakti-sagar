package com.blazestars.bhaktisagar.CommonActivities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.blazestars.bhaktisagar.Network.MyRequestQueue;
import com.blazestars.bhaktisagar.Network.VolleyData;
import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.SpacingItemDecoration;
import com.blazestars.bhaktisagar.myads.MyInterstitialAd;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.firebase.messaging.FirebaseMessaging;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.blazestars.bhaktisagar.Helper.AnalyticsHelper;
import com.blazestars.bhaktisagar.entity.Category;
import com.blazestars.bhaktisagar.entity.NameConstant;
import com.blazestars.bhaktisagar.entity.Story;
import com.blazestars.bhaktisagar.Helper.Helper;



public class MainActivity extends ActionBarActivity {

    Toolbar toolbar;
    Context context;
    private DrawerLayout mDrawerLayout;
    RelativeLayout mDrawerRelativelayout;
    ActionBarDrawerToggle actionBarDrawerToggle;

    ArrayList<Category> categories = new ArrayList<Category>();
    private Handler mHandler = new Handler();
    ProgressBar progressBar;
    TextView noDataTV;
    //RecyclerViewMenuAdapter recyclerViewMenuHomePageAdapter;
    RecyclerViewStoryAdapter recyclerViewStoryAdapter;
    RecyclerViewMenuAdapter recyclerViewMenuAdapter;
    Category currentCategory;
    ArrayList<Story> stories = new ArrayList<Story>();

    //RecyclerView Variables starts here
    //RecyclerView itemsRecyclerView;
    RecyclerView itemsRecyclerViewStory;
    RecyclerView mDrawerList;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;
    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = false; // True if we are still waiting for the last set of data to load.

    private boolean isLoadingMoreData = false;
    //private static boolean videoPlayed = false;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private int current_page = 1;
    //RecyclerView Variables ends here
    MyInterstitialAd interstitialAd;
    int storyClickCount = 0;
    String categoryNameNotification = "";
    String categoryIdNotification = "";
    TelephonyManager mTelephonyManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            FirebaseMessaging.getInstance().subscribeToTopic("notification_bollywood_v2");
        }catch (Exception e){

        }
        context = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.home_category_name));
        categoryIdNotification = getIntent().getStringExtra("category_id");
        categoryNameNotification = getIntent().getStringExtra("category_name");
        loadResources();
        setSideDrawer();
        // show alert if exist
        Helper.showRatingDialog(this);
    }

     private void setSideDrawer(){
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,toolbar,R.string.app_name,R.string.app_name);
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
         if (!TextUtils.isEmpty(categoryIdNotification) && !TextUtils.isEmpty(categoryNameNotification)) {
             loadMenu(true);
         }else {
             loadMenu(true);
             loadMenu(false);
         }

    }

    @Override
    public void onBackPressed() {
        /*if (itemsRecyclerViewStory != null){
            if (itemsRecyclerViewStory.getVisibility() == View.VISIBLE){
                itemsRecyclerViewStory.setVisibility(View.INVISIBLE);
                itemsRecyclerView.setVisibility(View.VISIBLE);
                loadMenu(true);
            }else {
                super.onBackPressed();
            }
        }else {*/
        try {
            super.onBackPressed();
        }catch (Exception e){
            e.printStackTrace();
            finish();
        }
        //}
    }

    private void loadMenu(final boolean isCached){
        MyRequestQueue.Instance(this).cancelPendingRequests("menu");
        new VolleyData(this){
            @Override
            protected void VPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected void VResponse(JSONObject response, String tag) {
                progressBar.setVisibility(View.GONE);
                JSONObject json = response;
                try {
                    if (json != null) {
                        try {
                            JSONArray JSONCategories = json.getJSONArray("categories");
                            recyclerViewMenuAdapter.clear();
                            //recyclerViewMenuHomePageAdapter.clear();
                            ArrayList<Category> menuCategories = new ArrayList<Category>();
                            for (int i=0;i<JSONCategories.length();i++){
                                menuCategories.add(new Category(JSONCategories.getJSONObject(i)));
                            }
                            //recyclerViewMenuHomePageAdapter.insertCategories(menuCategories);
                            menuCategories.add(0,new Category("all", getString(R.string.home_category_name),NameConstant.CATEGORY_TYPE_HOME,getString(R.string.home_category_name)));
                            currentCategory = menuCategories.get(0);
                            menuCategories.add(new Category("", getString(R.string.my_bookmarks), NameConstant.CATEGORY_TYPE_MY_NOTES,getString(R.string.my_bookmarks)));
                            //menuCategories.add(new Category("", "About Us", NameConstant.CATEGORY_TYPE_ABOUT_US));
                            menuCategories.add(new Category("", getString(R.string.settings), NameConstant.CATEGORY_TYPE_SETTINGS,getString(R.string.settings)));
                            //itemsRecyclerView.setVisibility(View.VISIBLE);
                            recyclerViewMenuAdapter.insertCategories(menuCategories);
                            if (!TextUtils.isEmpty(categoryIdNotification) && !TextUtils.isEmpty(categoryNameNotification)){
                                Category category = new Category(categoryIdNotification,categoryNameNotification,"",categoryNameNotification);
                                loadCategory(category,1,false,false,true);
                                loadCategory(category,1,false,false,false);
                                Log.d("category_id 3",categoryIdNotification);
                                Log.d("category_name 3",categoryNameNotification);
                                categoryIdNotification = "";
                                categoryNameNotification = "";
                            }else {
                                loadCategory(menuCategories.get(0),1,false,false,isCached);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            @Override
            protected void VError(VolleyError error, String tag) {
                progressBar.setVisibility(View.GONE);
            }
        }.getJsonObject(NameConstant.CATEGORY_URL, "menu",this,isCached);
    }

    public class settingsTapped implements Runnable {
        public void run() {
            Intent intent = new Intent(context,SettingsActivity.class);
            startActivity(intent);

        }
    }
    public class aboutUsTapped implements Runnable {
        public void run() {

            Intent intent = new Intent(context,AboutUs.class);
            intent.putExtra("header","aboutus");
            startActivity(intent);
        }
    }

    public class OpenNewCategoryThread implements Runnable {
        Category category;
        public OpenNewCategoryThread(Category category){
            this.category = category;
        }
        public void run() {
            try {
                String categoryName = "current_affairs";
                String label = "/Category opened = " + category.name;
                String action = "Opened";
                AnalyticsHelper.trackEvent(categoryName,action,label,MainActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("category opened",category.name);
            loadCategory(category, 1, false, false,true);
            loadCategory(category, 1, false, false,false);
        }
    }

    public class myNotesTapped implements Runnable {
        public void run() {
            Intent intent = new Intent(context,BookmarkActivity.class);
            startActivity(intent);
        }
    }


    public void menuItemClicked(Category category){
        mDrawerLayout.closeDrawer(mDrawerRelativelayout);
        if (category.type.equalsIgnoreCase(NameConstant.CATEGORY_TYPE_MY_NOTES)) {
            mHandler.postDelayed(new myNotesTapped(), 300);
            return;
        } else if (category.type.equalsIgnoreCase(NameConstant.CATEGORY_TYPE_ABOUT_US)) {
            mHandler.postDelayed(new aboutUsTapped(), 300);
            return;
        } else if (category.type.equalsIgnoreCase(NameConstant.CATEGORY_TYPE_SETTINGS)) {
            mHandler.postDelayed(new settingsTapped(), 300);
            return;
        }
        if (currentCategory.equals(category))
            return;
        mHandler.postDelayed(new OpenNewCategoryThread(category), 300);
    }

    private void loadCategory(Category category, final int page,boolean isRefreshing,boolean isLoadingMore,boolean isCached){
        if (category==null){
            noDataTV.setVisibility(View.VISIBLE);
            //itemsRecyclerView.setVisibility(View.INVISIBLE);
            itemsRecyclerViewStory.setVisibility(View.INVISIBLE);
            return;
        }
        currentCategory = category;
        getSupportActionBar().setTitle(category.name);
        /*if (category.type.equalsIgnoreCase(NameConstant.CATEGORY_TYPE_HOME)){
            itemsRecyclerView.setVisibility(View.VISIBLE);
            itemsRecyclerViewStory.setVisibility(View.GONE);
            loadMenu(isCached);
            Log.d("load category",category.name);
            //swipeRefreshLayout.setRefreshing(false);
            return;
        }else {*/
            Log.d("load category",category.name);
            //itemsRecyclerView.setVisibility(View.GONE);
            itemsRecyclerViewStory.setVisibility(View.VISIBLE);
            //itemsRecyclerView.setAdapter(recyclerViewStoryAdapter);
        //}
        current_page = page;
        noDataTV.setVisibility(View.GONE);
        if (isLoadingMore || isRefreshing){
            progressBar.setVisibility(View.GONE);
        }else {
            progressBar.setVisibility(View.VISIBLE);
            //itemsRecyclerView.setVisibility(View.INVISIBLE);
            //recyclerViewStoryAdapter.clear();
        }
        String URL = getCategoryStoryURL(category,page);
        MyRequestQueue.Instance(this).cancelPendingRequests("categoryStories");
        new VolleyData(this){
            @Override
            protected void VPreExecute() {

            }

            @Override
            protected void VResponse(JSONObject response, String tag) {
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                itemsRecyclerViewStory.setVisibility(View.VISIBLE);
                noDataTV.setVisibility(View.GONE);
                endLoadMore();
                JSONObject json = response;
                try {
                    if (json != null) {
                        try {
                            JSONArray JSONStories = json.getJSONArray("stories");
                            if (page==1) {
                                recyclerViewStoryAdapter.clear();
                                try {
                                    itemsRecyclerViewStory.scrollToPosition(0);
                                }catch (Exception ex){
                                    ex.printStackTrace();
                                }
                            }
                            ArrayList<Story> stories = new ArrayList<Story>();
                            for (int i=0;i<JSONStories.length();i++){
                                Story story = new Story(context,JSONStories.getJSONObject(i));
                                if (!isStoryAlreadyAdded(story))
                                    stories.add(story);
                            }
                            recyclerViewStoryAdapter.insertStories(stories);
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (stories.size()==0){
                                itemsRecyclerViewStory.setVisibility(View.GONE);
                                noDataTV.setVisibility(View.VISIBLE);
                            }
                            if (current_page>1){
                                current_page -=1;
                            }
                        }
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                    if (stories.size()==0){
                        itemsRecyclerViewStory.setVisibility(View.GONE);
                        noDataTV.setVisibility(View.VISIBLE);
                    }
                    if (current_page>1){
                        current_page -=1;
                    }
                }
            }

            @Override
            protected void VError(VolleyError error, String tag) {
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                endLoadMore();
                if (current_page==1){
                    itemsRecyclerViewStory.setVisibility(View.GONE);
                    noDataTV.setVisibility(View.VISIBLE);
                }
                if (current_page>1){
                    current_page -=1;
                }
            }
        }.getJsonObject(URL, "categoryStories", this,isCached);
    }



    private void openDetailPage(int position){
        HashMap<String,String> params = new HashMap<>();
        params.put("story_id",stories.get(position).postId);
        params.put("view_position",String.valueOf(position + 1));
        if (currentCategory != null && currentCategory.categoryId != null
                && currentCategory.categoryId.equalsIgnoreCase("all")){
            params.put("isHome","1");
        }else {
            params.put("isHome","0");
        }
        new VolleyData(this){
            @Override
            protected void VPreExecute() {

            }

            @Override
            protected void VResponse(JSONObject response, String tag) {

            }

            @Override
            protected void VError(VolleyError error, String tag) {

            }
        }.getPOSTJsonObject(NameConstant.POST_STORY_VIEWED_URL,"storyView",params);
        if (storyClickCount%3 == 0) {
            interstitialAd = new MyInterstitialAd(this, false);
        }
        storyClickCount++;
        if(!stories.get(position).youtubeVideoId.equalsIgnoreCase("")){
            try {
                Intent intent = YouTubeStandalonePlayer.createVideoIntent(this, NameConstant.DATA_V3_GOOGLE_DEVELOPER_KEY, stories.get(position).youtubeVideoId, 0, true, false);
                startActivity(intent);
                try {
                    String categoryName = "Video Played";
                    String label = stories.get(position).title;
                    String action = stories.get(position).postId;
                    AnalyticsHelper.trackEvent(categoryName, action, label, this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }catch (Exception ex){
                Toast.makeText(this,"Unable to play video, Go to settings and report the issue",Toast.LENGTH_LONG).show();
            }
        }else {
            Intent intent = new Intent(context, StoryDetailActivity.class);
            intent.putExtra("position", String.valueOf(position));
            //intent.putParcelableArrayListExtra("storiesArray", stories);
            StoryDetailActivity.storiesArray = stories;
            if (currentCategory != null) {
                intent.putExtra("category", currentCategory);
            }
            startActivityForResult(intent, NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL);
        }
    }

    private String getCategoryStoryURL(Category category,int page){
        return NameConstant.GET_STORIES_URL + "category="+category.categoryId+"&page=" + String.valueOf(page);
    }

    private void startLoadMore() {
        isLoadingMoreData = true;
        if (recyclerViewStoryAdapter!=null)
            recyclerViewStoryAdapter.insertMoreLoading();
    }

    private void endLoadMore() {
        isLoadingMoreData = false;
        if (recyclerViewStoryAdapter!=null)
            recyclerViewStoryAdapter.removeMoreLoading();
    }

    private void setItemsRecyclerViewDecoration(){
        if (itemsRecyclerViewStory!=null) {
            try {
                itemsRecyclerViewStory.removeItemDecoration(SpacingItemDecoration.potraitSpacingItemDecoration);
            } catch (Exception ex) {

            }
            try {
                itemsRecyclerViewStory.removeItemDecoration(SpacingItemDecoration.landscapeSpacingItemDecoration);
            } catch (Exception ex) {

            }
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                staggeredGridLayoutManager.setSpanCount(2);
                itemsRecyclerViewStory.addItemDecoration(SpacingItemDecoration.landscapeSpacingItemDecoration);
            } else {
                staggeredGridLayoutManager.setSpanCount(1);
                itemsRecyclerViewStory.addItemDecoration(SpacingItemDecoration.potraitSpacingItemDecoration);
            }
        }
    }

    private void loadResources(){
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerRelativelayout = (RelativeLayout) findViewById(R.id.left_drawer);
        mDrawerList = (RecyclerView) findViewById(R.id.left_drawer_list);
        RelativeLayout upperBar = (RelativeLayout) findViewById(R.id.upperBar);
        upperBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mDrawerList.setLayoutManager(new LinearLayoutManager(context));
        recyclerViewMenuAdapter = new RecyclerViewMenuAdapter(categories,context,false);
        recyclerViewMenuAdapter.setOnItemClickListener(new RecyclerViewMenuAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Category category = (Category) view.getTag();
                menuItemClicked(category);
            }
        });
        //recyclerViewMenuHomePageAdapter = new RecyclerViewMenuAdapter(new ArrayList<Category>(),context,true);
        /*recyclerViewMenuHomePageAdapter.setOnItemClickListener(new RecyclerViewMenuAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Category category = (Category) view.getTag();
                menuItemClicked(category);
            }
        });*/
        mDrawerList.setAdapter(recyclerViewMenuAdapter);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        noDataTV = (TextView)findViewById(R.id.no_data);
        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        //itemsRecyclerView = (RecyclerView)findViewById(R.id.itemsRecyclerView);
        itemsRecyclerViewStory = (RecyclerView)findViewById(R.id.itemsRecyclerViewStory);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                //refreshItems();
                previousTotal = 0;
                loadCategory(currentCategory, 1, true, false,false);
                //loadCategory(currentCategory, 1, true);
            }
        });
        //itemsRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        itemsRecyclerViewStory.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        //itemsRecyclerView.setAdapter(recyclerViewMenuHomePageAdapter);
        recyclerViewStoryAdapter = new RecyclerViewStoryAdapter(stories,context);
        recyclerViewStoryAdapter.setOnItemClickListener(new RecyclerViewStoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                openDetailPage(position);
            }
        });
        setItemsRecyclerViewDecoration();
        itemsRecyclerViewStory.setAdapter(recyclerViewStoryAdapter);
        itemsRecyclerViewStory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = recyclerView.getChildCount();
                int itemPositions[];
                totalItemCount = staggeredGridLayoutManager.getItemCount();
                //firstVisibleItem =
                        itemPositions = staggeredGridLayoutManager.findFirstVisibleItemPositions(new int[100]);
                firstVisibleItem = itemPositions[0];

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!isLoadingMoreData) {
                    if ((totalItemCount - visibleItemCount)
                            <= (firstVisibleItem + 1) && totalItemCount > 0) {
                        // End has been reached

                        // Do something
                        startLoadMore();
                        current_page++;
                        loadCategory(currentCategory,current_page,false,true,false);
                        //loading = true;
                    }
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (actionBarDrawerToggle != null) {
            actionBarDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setItemsRecyclerViewDecoration();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        Drawable searchLight = new IconDrawable(this, Iconify.IconValue.fa_search )
                .colorRes(R.color.white)
                .actionBarSize();
        menu.findItem(R.id.action_search).setIcon(searchLight);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (actionBarDrawerToggle != null) {
            if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
        }
        //noinspection SimplifiableIfStatement
        switch (id)
        {
            case R.id.action_search:
                searchButtonTapped();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void searchButtonTapped(){
        Intent intent = new Intent(getBaseContext(), SearchActivity.class);
        startActivityForResult(intent, NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL);
    }

    private boolean isStoryAlreadyAdded(Story story){
        for (int i=0;i<stories.size();i++){
            if (story.postId.equalsIgnoreCase(stories.get(i).postId))
                return true;
        }
        return false;
    }

}
