package com.blazestars.bhaktisagar.CommonActivities;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.SearchedCityPanchang;
import com.blazestars.bhaktisagar.entity.Story;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by narayan on 10/6/15.
 */
public class SearchCitiesPanchangAdaptor extends RecyclerView.Adapter<SearchCitiesPanchangAdaptor.ViewHolder> {
    private List<SearchedCityPanchang> cities;
    RecyclerViewMenuAdapter.OnItemClickListener mItemClickListener;

    public SearchCitiesPanchangAdaptor(List<SearchedCityPanchang> cities) {
        this.cities = cities;
        removeDuplicateCities();
    }

    @Override
    public SearchCitiesPanchangAdaptor.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_result
                , parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchCitiesPanchangAdaptor.ViewHolder holder, int position) {
        String title = getTitleFromCityObject(cities.get(position));
        holder.title.setText(title);
        holder.itemView.setTag(cities.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        if (cities.size() > 5)
            return 5;
        return cities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        View itemView;
        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            title = (TextView) itemView.findViewById(R.id.searchResultItem);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v,getPosition());
            }
        }
    }

    public void insertData(List<SearchedCityPanchang> cities){
        for (SearchedCityPanchang city : cities) {
            this.cities.add(city);
        }
        removeDuplicateCities();
        notifyDataSetChanged();
    }

    public void clearData(){
        cities.clear();
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(final RecyclerViewMenuAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private void removeDuplicateCities(){
        List<SearchedCityPanchang> tempCities = cities;
        for (int i = 0; i < cities.size(); i++){
            for (int j = i + 1; j < tempCities.size(); j++){
                if (getTitleFromCityObject(cities.get(i)).equalsIgnoreCase(getTitleFromCityObject(tempCities.get(j)))){
                    cities.remove(i);
                    i--;
                    break;
                }
            }
        }
    }

    private String getTitleFromCityObject(SearchedCityPanchang cityPanchang){
        String title = "";
        if (!TextUtils.isEmpty(cityPanchang.place)){
            title = cityPanchang.place;
        }
        if (!TextUtils.isEmpty(cityPanchang.state)){
            if (!TextUtils.isEmpty(title)){
                title += ", ";
            }
            title += cityPanchang.state;
        }
        if (!TextUtils.isEmpty(cityPanchang.country)){
            if (!TextUtils.isEmpty(title)){
                title += ", ";
            }
            title += cityPanchang.country;
        }
        return title;
    }
}
