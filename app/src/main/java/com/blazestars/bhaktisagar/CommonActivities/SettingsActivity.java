package com.blazestars.bhaktisagar.CommonActivities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

import com.blazestars.bhaktisagar.Helper.AnalyticsHelper;
import com.blazestars.bhaktisagar.Helper.Helper;
import com.blazestars.bhaktisagar.R;

public class SettingsActivity extends ActionBarActivity {

    ListView lv;
    ArrayList<String> listItems;
    Context context;
    Switch.OnCheckedChangeListener onOffChangeListner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.settings));
        context = this;
        onOffChangeListner = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked)
                    Helper.enablePushNotification(context, false);
                else
                    Helper.enablePushNotification(context,true);
            }
        };
        lv = (ListView) findViewById(R.id.settingList);
        setListItems();
        lv.setAdapter(new settingsListAdapter(this,R.layout.settings_list_item,listItems));
        setSettingListOnClickListner();
    }


    public void backButtonTapped(View v)
    {
        super.onBackPressed();
    }

    private void setListItems(){
        listItems = new ArrayList<String>();
        listItems.add(getString(R.string.choose_language));
        listItems.add("Feedback");
        listItems.add("Notifications");
        listItems.add("Rate Us");
        listItems.add("About Us");
    }

    private void setSettingListOnClickListner(){
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (listItems.get(i).equalsIgnoreCase(getString(R.string.choose_language))) {
                    Intent languangeChooser = new Intent(SettingsActivity.this,LanguageChooserActivity.class);
                    SharedPreferences preferences = getSharedPreferences("languageSelected", Context.MODE_PRIVATE);
                    languangeChooser.putExtra("languange_id",preferences.getString("languageSelected",""));
                    startActivity(languangeChooser);
                    try {
                        String categoryName = "language";
                        String label = "/language_chooser";
                        String action = "opened";
                        AnalyticsHelper.trackEvent(categoryName,action,label,SettingsActivity.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else if (listItems.get(i).equalsIgnoreCase("Feedback")){
                    Intent Email = new Intent(Intent.ACTION_SEND);
                    Email.setType("text/email");
                    Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"allbhaktiserials@gmail.com"});
                    Email.putExtra(Intent.EXTRA_SUBJECT, "Feedback For "+ getString(R.string.app_name)+" android application");
                    //AnalyticsHelper.MobileInfo mobileInfo = new AnalyticsHelper.MobileInfo(context);
                    //Email.putExtra(Intent.EXTRA_TEXT, "\tDevice Info :\n\tModel Name : " + mobileInfo.getMobileName() + "\n\tAndroid OS Version : " + mobileInfo.getMobileOs()+ "\n\tMobile Resolution : " + mobileInfo.getMobileResolution()+ "\n\tManufacturer : " + mobileInfo.getManufacturer()+ "\n\tApplication Version : " + mobileInfo.getappVersionName()+"\n\n");
                    startActivity(Intent.createChooser(Email, "Send Feedback:"));
                }else if (listItems.get(i).equalsIgnoreCase("Rate Us")){
                    Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

                    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                            Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    try {
                        startActivity(goToMarket);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class settingsListAdapter extends ArrayAdapter<String>{
        Context context;
        ArrayList<String> data;
        public settingsListAdapter(Context context,int layoutResourceId,ArrayList<String> data){
            super(context,layoutResourceId,data);
            this.context = context;
            this.data =data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (data.get(position).equalsIgnoreCase("About Us")){
                convertView = inflater.inflate(R.layout.settings_list_last_item, parent, false);
                TextView privacyPolicy = (TextView)convertView.findViewById(R.id.privacy_policy);
                TextView termsAndConditions = (TextView)convertView.findViewById(R.id.terms_and_conditions);
                privacyPolicy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,AboutUs.class);
                        intent.putExtra("header","privacypolicy");
                        startActivity(intent);
                    }
                });
                termsAndConditions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,AboutUs.class);
                        intent.putExtra("header","termsandconditions");
                        startActivity(intent);
                    }
                });
                return convertView;
            }else if(data.get(position).equalsIgnoreCase("Feedback")) {
                TextView tv_item;
                convertView = inflater.inflate(R.layout.settings_list_item, parent, false);
                tv_item = (TextView) convertView.findViewById(R.id.item);
                tv_item.setText(getString(R.string.feedback));
                return convertView;
            }else if(data.get(position).equalsIgnoreCase(getString(R.string.choose_language))) {
                TextView tv_item;
                convertView = inflater.inflate(R.layout.settings_list_item, parent, false);
                tv_item = (TextView) convertView.findViewById(R.id.item);
                tv_item.setText(getString(R.string.choose_language));
                return convertView;
            }else if(data.get(position).equalsIgnoreCase("Rate Us")) {
                TextView tv_item;
                convertView = inflater.inflate(R.layout.settings_list_item, parent, false);
                tv_item = (TextView) convertView.findViewById(R.id.item);
                tv_item.setText(getString(R.string.rate_us));
                return convertView;
            }
            else if (data.get(position).equalsIgnoreCase("Notifications"))
            {
                TextView tv_item;
                convertView = inflater.inflate(R.layout.settings_push_notification_item, parent, false);
                tv_item = (TextView) convertView.findViewById(R.id.item);
                tv_item.setText(getString(R.string.notification));
                final Switch onOff = (Switch) convertView.findViewById(R.id.onOff);
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onOff.toggle();
                    }
                });
                onOffChangeListner = new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (!isChecked) {
                            if (Helper.isPushNotificationEnabled(context)) {
                                Helper.enablePushNotification(context, false);
                                Toast.makeText(context, "Notification Disabled", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (!Helper.isPushNotificationEnabled(context)) {
                                Helper.enablePushNotification(context, true);
                                Toast.makeText(context, "Notification Enabled", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                };
                onOff.setOnCheckedChangeListener(onOffChangeListner);
                if (Helper.isPushNotificationEnabled(context)) {
                    onOff.setChecked(true);
                }
                else {
                    onOff.setChecked(false);
                }
                return convertView;

            }
            return null;
        }

    }
}
