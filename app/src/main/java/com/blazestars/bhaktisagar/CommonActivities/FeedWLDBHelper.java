package com.blazestars.bhaktisagar.CommonActivities;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import org.json.JSONObject;
import com.blazestars.bhaktisagar.entity.Story;

public class FeedWLDBHelper extends SQLiteOpenHelper{
	 public static final String DATABASE_NAME = "FeedWLAndroid.db";
	 
	 public FeedWLDBHelper(Context context) {
   	  super(context, DATABASE_NAME, null, 5);
   //	  tableName = TableName;
         
   }
	 
	 @Override
	    public void onCreate(SQLiteDatabase database) {
	      String query;
	      query = "CREATE TABLE IF NOT EXISTS Bookmarked(postId TEXT,storyJSONString TEXT)";
	      database.execSQL(query);
	 }
	    @Override
	    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
	        onCreate(database);
	    }

   	    public void removeFromBookmarked(String postId) {
	        SQLiteDatabase database = this.getWritableDatabase();   
	        String deleteQuery = "DELETE FROM  Bookmarked where postId='"+ postId +"'";
	        database.execSQL(deleteQuery);
	    }

	public ArrayList<Story> getAllPostsBookmarked(Context context) {
		ArrayList<Story> bookmarkedStories = new ArrayList<Story>();
		String selectQuery = "SELECT  * FROM Bookmarked";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				String storyJSONString = cursor.getString(1);
				Story story=null;
				try {
					story = new Story(context,new JSONObject(storyJSONString));
				}catch (Exception ex){
					ex.printStackTrace();
				}
				if(story!=null)
					bookmarkedStories.add(story);
			}while (cursor.moveToNext());
		}
		return bookmarkedStories;
	}

	    public Boolean checkIfBookmarked(String postId) {
	        String selectQuery = "SELECT  * FROM Bookmarked where postId='"+ postId +"'";
	        SQLiteDatabase database = this.getWritableDatabase();
	        Cursor cursor = database.rawQuery(selectQuery, null);
	        if (cursor.moveToFirst()) {
	            do {
	          return true;
	            } while (cursor.moveToNext());
	        }
	      
	        // return contact list
	        return false;
	    }


	    public void addToBookmarked(Story story) {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("postId",story.postId);
            values.put("storyJSONString",story.storyJSONString);
            database.insert("Bookmarked", null, values);
            database.close();
        }
}

