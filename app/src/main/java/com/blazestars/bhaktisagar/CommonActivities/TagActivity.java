package com.blazestars.bhaktisagar.CommonActivities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.blazestars.bhaktisagar.Network.MyRequestQueue;
import com.blazestars.bhaktisagar.Network.VolleyData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.NameConstant;
import com.blazestars.bhaktisagar.entity.SpacingItemDecoration;
import com.blazestars.bhaktisagar.entity.Story;


public class TagActivity extends ActionBarActivity {

    Toolbar toolbar;
    Context context;
    ProgressBar progressBar;
    RecyclerViewStoryAdapter recyclerViewStoryAdapter;
    ArrayList<Story> stories = new ArrayList<Story>();
    Tag tag;

    //RecyclerView Variables starts here
    RecyclerView itemsRecyclerView;
    TextView noDataTV;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;
    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = false; // True if we are still waiting for the last set of data to load.

    private boolean isLoadingMoreData = false;
    private int visibleThreshold = 5; // The minimum amount of items to have below your current scroll position before loading more.
    int firstVisibleItem, visibleItemCount, totalItemCount;
    private int current_page = 1;
    //RecyclerView Variables ends here

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag);
        context = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tag = (Tag)getIntent().getExtras().get("tag");
        //getSupportActionBar().setTitle(tag.name);
        loadResources();
        try {
            String categoryName = "Tag";
            //String label = "/Tag opened with id" + tag.tagId + "/name = " + tag.name;
            String action = "Opened";
            //AnalyticsHelper.trackEvent(categoryName,action,label,TagActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadStoryWithTag(1, false, false);
    }

    private void setItemsRecyclerViewDecoration(){
        if (itemsRecyclerView!=null) {
            try {
                itemsRecyclerView.removeItemDecoration(SpacingItemDecoration.potraitSpacingItemDecoration);
            } catch (Exception ex) {

            }
            try {
                itemsRecyclerView.removeItemDecoration(SpacingItemDecoration.landscapeSpacingItemDecoration);
            } catch (Exception ex) {

            }
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                staggeredGridLayoutManager.setSpanCount(2);
                itemsRecyclerView.addItemDecoration(SpacingItemDecoration.landscapeSpacingItemDecoration);
            } else {
                staggeredGridLayoutManager.setSpanCount(1);
                itemsRecyclerView.addItemDecoration(SpacingItemDecoration.potraitSpacingItemDecoration);
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setItemsRecyclerViewDecoration();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode==NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL){
                if (resultCode==RESULT_OK){
                    if (data!=null){
                        setResult(RESULT_OK,data);
                        finish();
                    }
                }
            }
        }catch (Exception ex){

        }
    }

    private void loadStoryWithTag(final int page,boolean isRefreshing,boolean isLoadingMore){
        current_page = page;
        noDataTV.setVisibility(View.INVISIBLE);
        if (isLoadingMore || isRefreshing){
            progressBar.setVisibility(View.GONE);
        }else {
            progressBar.setVisibility(View.VISIBLE);
            itemsRecyclerView.setVisibility(View.VISIBLE);
            recyclerViewStoryAdapter.clear();
        }
        if (isRefreshing){

        }else if (isLoadingMore){

        }
        String URL = getStoryWithTagURL(page);
        MyRequestQueue.Instance(this).cancelPendingRequests("tagStories");
        new VolleyData(this){
            @Override
            protected void VPreExecute() {

            }

            @Override
            protected void VResponse(JSONObject response, String tag) {
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                itemsRecyclerView.setVisibility(View.VISIBLE);
                endLoadMore();
                JSONObject json = response;
                try {
                    if (json != null) {
                        try {
                            JSONArray JSONStories = json.getJSONArray("stories");
                            if (page==1)
                                recyclerViewStoryAdapter.clear();
                            ArrayList<Story> stories = new ArrayList<Story>();
                            for (int i=0;i<JSONStories.length();i++){
                                Story story = new Story(context,JSONStories.getJSONObject(i));
                                if (!isStoryAlreadyAdded(story))
                                    stories.add(story);
                            }
                            recyclerViewStoryAdapter.insertStories(stories);
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (stories.size()==0){
                                itemsRecyclerView.setVisibility(View.GONE);
                                noDataTV.setVisibility(View.VISIBLE);
                            }
                            if (current_page>1){
                                current_page -=1;
                            }
                        }
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                    if (stories.size()==0){
                        itemsRecyclerView.setVisibility(View.GONE);
                        noDataTV.setVisibility(View.VISIBLE);
                    }
                    if (current_page>1){
                        current_page -=1;
                    }
                }
            }

            @Override
            protected void VError(VolleyError error, String tag) {
                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                endLoadMore();
                if (stories.size()==0){
                    itemsRecyclerView.setVisibility(View.GONE);
                    noDataTV.setVisibility(View.VISIBLE);
                }
                if (current_page>1){
                    current_page -=1;
                }
            }
        }.getJsonObject(URL, "tagStories",this,false);
    }

    private void openDetailPage(int position){
        Intent intent = new Intent(context, StoryDetailActivity.class);
        intent.putExtra("position", String.valueOf(position));
        //intent.putParcelableArrayListExtra("storiesArray", stories);
        StoryDetailActivity.storiesArray = stories;
        startActivityForResult(intent, NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL);
    }

    private String getStoryWithTagURL(int page){
        return "";//NameConstant.GET_STORIES_URL + "tag="+tag.tagId+"&page=" + String.valueOf(page);
    }

    private void startLoadMore() {
        isLoadingMoreData = true;
        if (recyclerViewStoryAdapter!=null)
            recyclerViewStoryAdapter.insertMoreLoading();
    }

    private void endLoadMore() {
        isLoadingMoreData = false;
        if (recyclerViewStoryAdapter!=null)
            recyclerViewStoryAdapter.removeMoreLoading();
    }

    private void loadResources(){
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        itemsRecyclerView = (RecyclerView)findViewById(R.id.itemsRecyclerView);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);

        noDataTV = (TextView)findViewById(R.id.no_data);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                //refreshItems();
                previousTotal = 0;
                loadStoryWithTag(1, true, false);
            }
        });
        setItemsRecyclerViewDecoration();
        itemsRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerViewStoryAdapter = new RecyclerViewStoryAdapter(stories,context);
        recyclerViewStoryAdapter.setOnItemClickListener(new RecyclerViewStoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                openDetailPage(position);
            }
        });
        itemsRecyclerView.setAdapter(recyclerViewStoryAdapter);
        itemsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = staggeredGridLayoutManager.getItemCount();
                int itemPosition[];
                itemPosition = staggeredGridLayoutManager.findFirstVisibleItemPositions(new int[100]);
                firstVisibleItem = itemPosition[0];

                /*if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }*/
                if (!isLoadingMoreData) {
                    if ((totalItemCount - visibleItemCount)
                            <= (firstVisibleItem + 1) && totalItemCount > 0) {
                        // End has been reached

                        // Do something
                        startLoadMore();
                        current_page++;
                        loadStoryWithTag(current_page, false, true);
                        //loading = true;
                    }
                }
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about_us, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isStoryAlreadyAdded(Story story){
        for (int i=0;i<stories.size();i++){
            if (story.postId.equalsIgnoreCase(stories.get(i).postId))
                return true;
        }
        return false;
    }
}
