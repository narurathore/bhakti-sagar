package com.blazestars.bhaktisagar.CommonActivities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.Story;

/**
 * Created by narayan on 10/6/15.
 */
public class SearchNewsSuggestionAdaptor extends ArrayAdapter<Story> {
    private Context mContext;
    private ArrayList<Story> stories;

    private final LayoutInflater inflater;

    public SearchNewsSuggestionAdaptor(final Context context, int resource, ArrayList<Story> stories) {
        super(context, resource, stories);
        this.stories = stories;
        this.mContext = context;
        this.inflater=LayoutInflater.from(context);

    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        return stories.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        TextView tv;
        if (convertView==null) {
            convertView = inflater.inflate(R.layout.search_result, null);
        }
        tv = (TextView)convertView.findViewById(R.id.searchResultItem);
        tv.setText(stories.get(position).title);
        convertView.setTag(stories.get(position));
        return convertView;
    }
}
