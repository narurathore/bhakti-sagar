package com.blazestars.bhaktisagar.CommonActivities;


import android.content.Context;

import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.blazestars.bhaktisagar.R;
import com.google.firebase.messaging.FirebaseMessaging;


public class SplashScreen extends ActionBarActivity {

    private Handler mHandler = new Handler();

    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        context = this;
        //AdColony.configure(this, "version:1.0,store:google", "appb5a860ddc92a4717a4", "vz418557d0726c499e90");
        SharedPreferences prefs1 = getSharedPreferences("appOpenCount", Context.MODE_PRIVATE);
        int appOpenCount = prefs1.getInt("appOpenCount", 0);
        appOpenCount++;
        SharedPreferences.Editor editor1 = getSharedPreferences("appOpenCount", Context.MODE_PRIVATE).edit();
        editor1.putInt("appOpenCount", appOpenCount);
        editor1.commit();
        mHandler.postDelayed(new LoadActivitiesThread(), 2000);
        try {
            FirebaseMessaging.getInstance().subscribeToTopic("notification_bollywood");
        }catch (Exception e){

        }
    }


    public class LoadActivitiesThread implements Runnable {
        public void run() {
            loadActivities();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }



    private void loadActivities(){
                Intent home = new Intent(this, MainHomePageActivity.class);
                startActivity(home);
                finish();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }
}
