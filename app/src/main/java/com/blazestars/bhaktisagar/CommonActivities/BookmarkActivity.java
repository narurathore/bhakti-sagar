package com.blazestars.bhaktisagar.CommonActivities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import com.android.volley.VolleyError;
import com.blazestars.bhaktisagar.Helper.AnalyticsHelper;
import com.blazestars.bhaktisagar.Network.VolleyData;
import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.NameConstant;
import com.blazestars.bhaktisagar.entity.SpacingItemDecoration;
import com.blazestars.bhaktisagar.entity.Story;
import com.blazestars.bhaktisagar.myads.MyInterstitialAd;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import org.json.JSONObject;


public class BookmarkActivity extends ActionBarActivity {
    Toolbar toolbar;
    Context context;
    ProgressBar progressBar;
    FeedWLDBHelper dbHelper = new FeedWLDBHelper(this);
    RecyclerViewStoryAdapter recyclerViewStoryAdapter;
    TextView noDataTV;
    ArrayList<Story> stories = new ArrayList<Story>();
    //RecyclerView Variables starts here
    RecyclerView itemsRecyclerView;
    StaggeredGridLayoutManager staggeredGridLayoutManager;
    //RecyclerView Variables ends here
    MyInterstitialAd interstitialAd;
    int storyClickCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_notes);
        context = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.bookmark_activity_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadResources();
        loadBookMarked();
        try {
            String categoryName = "my_bookmarks";
            String label = "/my_bookmarks opened" ;
            String action = "Opened";
            AnalyticsHelper.trackEvent(categoryName, action, label, BookmarkActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private void setItemsRecyclerViewDecoration(){
        if (itemsRecyclerView!=null) {
            try {
                itemsRecyclerView.removeItemDecoration(SpacingItemDecoration.potraitSpacingItemDecoration);
            } catch (Exception ex) {

            }
            try {
                itemsRecyclerView.removeItemDecoration(SpacingItemDecoration.landscapeSpacingItemDecoration);
            } catch (Exception ex) {

            }
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                staggeredGridLayoutManager.setSpanCount(2);
                itemsRecyclerView.addItemDecoration(SpacingItemDecoration.landscapeSpacingItemDecoration);
            } else {
                staggeredGridLayoutManager.setSpanCount(1);
                itemsRecyclerView.addItemDecoration(SpacingItemDecoration.potraitSpacingItemDecoration);
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setItemsRecyclerViewDecoration();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
        switch (requestCode) {
            case NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL:
                if (data == null) {
                    loadBookMarked();
                } else {
                    setResult(RESULT_OK, data);
                    finish();
                }
                break;
        }
        }catch (Exception ex){

        }

    }


    private void loadBookMarked(){
        itemsRecyclerView.setVisibility(View.VISIBLE);
        noDataTV.setVisibility(View.INVISIBLE);

        if (recyclerViewStoryAdapter!=null){
            stories.clear();
            recyclerViewStoryAdapter.notifyDataSetChanged();
            recyclerViewStoryAdapter.insertStories(dbHelper.getAllPostsBookmarked(this));
            if (stories.size()==0){
                itemsRecyclerView.setVisibility(View.GONE);
                noDataTV.setVisibility(View.VISIBLE);
            }
        }else {
            recyclerViewStoryAdapter = new RecyclerViewStoryAdapter(stories, context);
            recyclerViewStoryAdapter.insertStories(dbHelper.getAllPostsBookmarked(this));
            recyclerViewStoryAdapter.setOnItemClickListener(new RecyclerViewStoryAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    BookmarkActivity.this.openDetailPage(position);

                }
            });
            recyclerViewStoryAdapter.setOnItemLongClickListner(new RecyclerViewStoryAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, final int position) {
                    final Story selectedStory = stories.get(position);
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BookmarkActivity.this);

                    // set title
                    alertDialogBuilder.setTitle("Remove From My Bookmarks");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Are you sure you want to remove this story from My Bookmarks?")
                            .setCancelable(true)
                            .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dbHelper.removeFromBookmarked(selectedStory.postId);
                                    dialog.cancel();
                                    //gridArray.remove(position);
                                    if (stories.size() == 0) {
                                        //rl_no_data.setVisibility(View.VISIBLE);
                                    }
                                    recyclerViewStoryAdapter.remove(position);
                                    if (recyclerViewStoryAdapter.getItemCount() == 0) {
                                        itemsRecyclerView.setVisibility(View.GONE);
                                        noDataTV.setVisibility(View.VISIBLE);
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            });
            itemsRecyclerView.setAdapter(recyclerViewStoryAdapter);
        }
    }

    private void openDetailPage(int position){
        HashMap<String,String> params = new HashMap<>();
        params.put("story_id",stories.get(position).postId);
        params.put("view_position",String.valueOf(position + 1));
        params.put("isHome","4");
        new VolleyData(this){
            @Override
            protected void VPreExecute() {

            }

            @Override
            protected void VResponse(JSONObject response, String tag) {

            }

            @Override
            protected void VError(VolleyError error, String tag) {

            }
        }.getPOSTJsonObject(NameConstant.POST_STORY_VIEWED_URL,"storyView",params);
        if (storyClickCount%3 == 0) {
            interstitialAd = new MyInterstitialAd(this, false);
        }
        storyClickCount++;
        if(!stories.get(position).youtubeVideoId.equalsIgnoreCase("")){
            try {
                Intent intent = YouTubeStandalonePlayer.createVideoIntent(this, NameConstant.DATA_V3_GOOGLE_DEVELOPER_KEY, stories.get(position).youtubeVideoId, 0, true, false);
                startActivity(intent);
                try {
                    String categoryName = "Video Played";
                    String label = stories.get(position).title;
                    String action = stories.get(position).postId;
                    AnalyticsHelper.trackEvent(categoryName, action, label, this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }catch (Exception ex){
                Toast.makeText(this,"Unable to play video, Go to settings and report the issue",Toast.LENGTH_LONG).show();
            }
        }else {
            Intent intent = new Intent(this, StoryDetailActivity.class);
            intent.putExtra("position", String.valueOf(position));
            //intent.putParcelableArrayListExtra("storiesArray", stories);
            StoryDetailActivity.storiesArray = stories;
            startActivityForResult(intent, NameConstant.REQUEST_CODE_BACK_FROM_STORY_DETAIL);
        }
    }

    private void loadResources(){
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        noDataTV = (TextView) findViewById(R.id.no_data);
        itemsRecyclerView = (RecyclerView)findViewById(R.id.itemsRecyclerView);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        setItemsRecyclerViewDecoration();
        itemsRecyclerView.setLayoutManager(staggeredGridLayoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_about_us, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
