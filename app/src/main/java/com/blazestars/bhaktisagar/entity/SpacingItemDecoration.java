package com.blazestars.bhaktisagar.entity;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Admin on 07-01-2016.
 */
public class SpacingItemDecoration extends RecyclerView.ItemDecoration {
    private int space;
    public static SpacingItemDecoration potraitSpacingItemDecoration = new SpacingItemDecoration(0);
    public static SpacingItemDecoration landscapeSpacingItemDecoration = new SpacingItemDecoration(5);

    public SpacingItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;
        outRect.top = space;
    }
}
