package com.blazestars.bhaktisagar.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DailyPanchangModel implements Parcelable {
    public	String firstBody;
    public	String firstBodyStyle;
    public	String lastBodyStyle;
	public String title;
    public String lastBody;
    public ArrayList<CardDailyPanchangModel> cards;

    public DailyPanchangModel(JSONObject category)
    {
        super();
        try {
            firstBody = "";
            title = "";
            lastBody = "";
            firstBodyStyle = "";
            lastBodyStyle = "";
            this.firstBody = category.getString("firstBody");
            this.title = category.getString("title");
            this.lastBody = category.getString("lastBody");
            this.firstBodyStyle = category.getString("firstBodyStyle");
            this.lastBodyStyle = category.getString("lastBodyStyle");
            this.cards = new ArrayList<CardDailyPanchangModel>();
            JSONArray cardsArray = category.getJSONArray("cards");
            for (int i = 0; i<cardsArray.length(); i++) {
                this.cards.add(new CardDailyPanchangModel(cardsArray.getJSONObject(i)));
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(firstBody);
        dest.writeString(title);
        dest.writeString(lastBody);
        dest.writeList(cards);
        dest.writeString(firstBodyStyle);
        dest.writeString(lastBodyStyle);
    }


    public DailyPanchangModel(Parcel in) {
        firstBody = in.readString();
        title = in.readString();
        lastBody = in.readString();
        cards = in.readArrayList(CardDailyPanchangModel.class.getClassLoader());
        firstBodyStyle = in.readString();
        lastBodyStyle = in.readString();
    }

    public static final Creator<DailyPanchangModel> CREATOR = new Creator<DailyPanchangModel>() {
        public DailyPanchangModel createFromParcel(Parcel in) {
            return new DailyPanchangModel(in);
        }
        public DailyPanchangModel[] newArray(int size) {
            return new DailyPanchangModel[size];
        }
    };
}
