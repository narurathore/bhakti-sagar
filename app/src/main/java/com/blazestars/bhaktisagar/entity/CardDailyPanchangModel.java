package com.blazestars.bhaktisagar.entity;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

public class CardDailyPanchangModel implements Parcelable {
    public	String cardHTML;
	public String style;

    public CardDailyPanchangModel(JSONObject category)
    {
        super();
        try {
            cardHTML = "";
            style = "";
            this.cardHTML = category.getString("cardHTML");
            this.style = category.getString("style");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(cardHTML);
        dest.writeString(style);
    }


    public CardDailyPanchangModel(Parcel in) {
        cardHTML = in.readString();
        style = in.readString();
    }

    public static final Creator<CardDailyPanchangModel> CREATOR = new Creator<CardDailyPanchangModel>() {
        public CardDailyPanchangModel createFromParcel(Parcel in) {
            return new CardDailyPanchangModel(in);
        }
        public CardDailyPanchangModel[] newArray(int size) {
            return new CardDailyPanchangModel[size];
        }
    };
}
