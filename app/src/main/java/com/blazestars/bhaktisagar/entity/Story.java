package com.blazestars.bhaktisagar.entity;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONObject;

import java.nio.charset.Charset;

public class Story implements Parcelable {
    public String postId;
    public String title;
    public String titleHindi;
    public String body;
    public String fullImage;
    public String thumbImage;
    final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
    final Charset UTF_8 = Charset.forName("UTF-8");
    public String storyJSONString;
    public String youtubeVideoId;
    public boolean isAd;
    public Story(){
        this.title = "";
        this.body = "";
        this.fullImage = "";
        this.postId = "";
        isAd = false;
        thumbImage = "";
        storyJSONString = "";
        titleHindi = "";
    }
    public Story(Context context, JSONObject storyObj){
        try {
            storyJSONString = storyObj.toString();
            this.title = "";
            this.body = "";
            this.fullImage = "";
            this.postId = "";
            thumbImage = "";
            titleHindi = "";
            isAd = false;
            try {
                this.postId = storyObj.getString("id");
            }catch (Exception ex){
                this.postId = "";
            }
            try {
                this.title = storyObj.getString("title");

                byte ptext[] = title.getBytes(ISO_8859_1);
                title = new String(ptext, UTF_8);
            }catch (Exception ex){
                this.title = "";
            }
            try {
                this.titleHindi = storyObj.getString("title_hindi");
                if (TextUtils.isEmpty(titleHindi)){
                    titleHindi = title;
                }
            }catch (Exception ex){
                titleHindi = title;
            }
            try {
                this.body = storyObj.getString("body");
                byte ptext[] = body.getBytes(ISO_8859_1);
                body = new String(ptext, UTF_8);
            }catch (Exception ex) {
                this.body = "";
            }
            try{
                String postImage = storyObj.getString("imagePath");
                if(postImage!=null&& !postImage.equals(""))
                {
                    this.fullImage = postImage;
                }else {
                    this.fullImage = "";
                }
            }
            catch (Exception thumbEx)
            {
                this.fullImage = "";
            }
            try{
                thumbImage = storyObj.getString("thumbImage");

            }
            catch (Exception thumbEx)
            {
                this.thumbImage = "";
            }
            try{
                youtubeVideoId = storyObj.getString("youtubeVideoId");
                if(!youtubeVideoId.equalsIgnoreCase(""))
                    thumbImage = "http://img.youtube.com/vi/"+youtubeVideoId+"/hqdefault.jpg";
            }
            catch (Exception thumbEx)
            {
                youtubeVideoId = "";
            }
            if (fullImage.equalsIgnoreCase("")){
                fullImage = thumbImage;
            }
            if (thumbImage.equalsIgnoreCase("")){
                thumbImage = fullImage;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(title);
        dest.writeString(body);
        dest.writeString(fullImage);
        dest.writeString(postId);
        dest.writeString(storyJSONString);
        dest.writeString(youtubeVideoId);
        dest.writeString(thumbImage);
    }


    public Story(Parcel in) {
        title = in.readString();
        body = in.readString();
        fullImage = in.readString();
        postId = in.readString();
        storyJSONString = in.readString();
        youtubeVideoId = in.readString();
        thumbImage = in.readString();
    }

    public static final Creator<Story> CREATOR = new Creator<Story>() {
        public Story createFromParcel(Parcel in) {
            return new Story(in);
        }

        public Story[] newArray(int size) {
            return new Story[size];
        }
    };

}
