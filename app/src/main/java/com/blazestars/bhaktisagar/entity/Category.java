package com.blazestars.bhaktisagar.entity;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.StrictMode;
import android.text.TextUtils;

import org.json.JSONObject;

public class Category implements Parcelable {
    public	String categoryId;
	public String name;
    public String type;
    public String imagePath;
    public String nameHindi;

    public Category(String id, String name, String type, String nameHindi)
	{
		super();
		this.categoryId = id;
		this.name = name;
        this.type = type;
        this.nameHindi = nameHindi;
	}

    public Category(JSONObject category)
    {
        super();
        try {
            categoryId = "";
            name = "";
            type = "";
            nameHindi = "";
            this.categoryId = category.getString("id");
            this.name = category.getString("name");
            this.nameHindi = category.getString("name_hindi");
            if (TextUtils.isEmpty(nameHindi)){
                nameHindi = name;
            }
            this.imagePath = category.getString("imagePath");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(categoryId);
        dest.writeString(name);
        dest.writeString(type);
        dest.writeString(imagePath);
        dest.writeString(nameHindi);
    }


    public Category(Parcel in) {
        categoryId = in.readString();
        name = in.readString();
        type = in.readString();
        imagePath = in.readString();
        nameHindi = in.readString();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
