package com.blazestars.bhaktisagar.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.json.JSONObject;

/**
 * Created by narayansingh on 27/05/17.
 */

public class SearchedCityPanchang implements Parcelable {
    public	String id;
    public String state;
    public String place;
    public String country;

    public SearchedCityPanchang() {
    }

    public SearchedCityPanchang(JSONObject category)
    {
        super();
        try {
            id = "";
            place = "";
            state = "";
            country = "";
            this.id = category.getString("id");
            this.place = category.getString("place");
            this.state = category.getString("state");
            this.country = category.getString("country");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(id);
        dest.writeString(place);
        dest.writeString(state);
        dest.writeString(country);
    }


    public SearchedCityPanchang(Parcel in) {
        id = in.readString();
        place = in.readString();
        state = in.readString();
        country = in.readString();
    }

    public static final Creator<SearchedCityPanchang> CREATOR = new Creator<SearchedCityPanchang>() {
        public SearchedCityPanchang createFromParcel(Parcel in) {
            return new SearchedCityPanchang(in);
        }
        public SearchedCityPanchang[] newArray(int size) {
            return new SearchedCityPanchang[size];
        }
    };
}