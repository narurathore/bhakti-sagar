package com.blazestars.bhaktisagar.entity;

/**
 * Created by narayan on 1/8/15.
 */
public class NameConstant {

    public static final String GOOGLE_ANALYTICS_ID = "UA-89311702-1";
    public static final String DATA_V3_GOOGLE_DEVELOPER_KEY = "AIzaSyCRI6bluu6SwtwGhXsnmbtWoThApZNt9xA";
    public static final String BASE_URL = "http://blazestars.com/abs/api/";
    public static final String ALERT_NOTIFICATION_INTENT_EXTRA_NAME = "alert_message";
    public static final String LATEST_VERSION_URL = BASE_URL + "get_latest_version_code.php";
    public static final String NEW_FEATURES_URL = BASE_URL + "get_new_features.php?";
    public static final String CATEGORY_URL = BASE_URL + "getCategories.php";
    public static final String PLACE_URL_PANCHANG = BASE_URL + "panchang/getCities.php";
    public static final String URL_PANCHANG = BASE_URL + "panchang/getOneDayPanchang.php";
    public static final String GET_STORY_URL = BASE_URL + "getStoryDetail.php?";
    public static final String GET_SEARCH_URL = BASE_URL + "getSearchResult.php?";
    public static final String GET_SEARCH_SUGGESTIONS_URL = BASE_URL + "search_suggestions.php?";
    public static final String CATEGORY_TYPE_HOME = "home";
    public static final String CATEGORY_TYPE_PANCHANG = "panchang";
    public static final String CATEGORY_TYPE_MY_NOTES = "bookmark";
    public static final String CATEGORY_TYPE_ABOUT_US = "about us";
    public static final String HTTP_CLIENT_CACHE_DATABASE = "URL_CACHE";
    public static final String CATEGORY_TYPE_SETTINGS = "settings";
    public static final int REQUEST_CODE_BACK_FROM_STORY_DETAIL = 1;
    public static final String GET_STORIES_URL = BASE_URL + "getStoriesForCategory.php?";
    public static final String STORY_PAGE_FONT_SIZE_DATABASE_NAME = "storyPageFontSize";
    public static final String POST_STORY_VIEWED_URL = BASE_URL + "postStoryViewed.php?";
}
