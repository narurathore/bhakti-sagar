package com.blazestars.bhaktisagar.TextViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Piyush on 6/15/15.
 */
public class MenuTextView extends TextView{
    public MenuTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MenuTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MenuTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/menufont.ttf");

        setTypeface(tf);
    }
}
