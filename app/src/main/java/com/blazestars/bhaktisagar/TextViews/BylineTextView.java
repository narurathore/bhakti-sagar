package com.blazestars.bhaktisagar.TextViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Piyush on 6/15/15.
 */
public class BylineTextView extends TextView{
    public BylineTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public BylineTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BylineTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/bylinefont.ttf");

        setTypeface(tf);
    }
}
