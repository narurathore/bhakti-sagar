package com.blazestars.bhaktisagar.Helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.NameConstant;
import com.blazestars.bhaktisagar.entity.Story;

public class Helper {

	public static void openShareDialog(Story story, Context context){
		try{
			String title = "Title : "+story.title + "\n\n";
			String action = "read";
			if (story.youtubeVideoId!=null && !story.youtubeVideoId.equalsIgnoreCase("")){
				action = "watch";
			}
			String appLink = "To "+action+" in android application download "+context.getString(R.string.app_name)+" :\n http://play.google.com/store/apps/details?id=" + context.getPackageName()+"\n\n";
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, title + appLink);
			sendIntent.setType("text/plain");
			context.startActivity(sendIntent);
			try {
				String categoryName = "Story Share Dialog Opened";
				String label = story.title;
				String action2 = story.postId;
				AnalyticsHelper.trackEvent(categoryName, action2, label, (Activity) context);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}catch (Exception ex){

		}
	}

	public static void enablePushNotification(Context context,Boolean pushBool)
	{
		SharedPreferences.Editor editor = context.getSharedPreferences("PushNotificationEnabled", Context.MODE_PRIVATE).edit();
		editor.putBoolean("PushNotificationEnabled", pushBool);
		editor.commit();

	}

	public static String getCachedDataForUrl(String url,Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(NameConstant.HTTP_CLIENT_CACHE_DATABASE, context.MODE_PRIVATE);
		String data = sharedPreferences.getString(String.valueOf(url.hashCode()), "");
		return data;
	}

	public static void saveCacheDataForUrl(String url,String string,Context context){
		SharedPreferences.Editor sharedPrefencesEditor = context.getSharedPreferences(NameConstant.HTTP_CLIENT_CACHE_DATABASE,context.MODE_PRIVATE).edit();
		sharedPrefencesEditor.putString(String.valueOf(url.hashCode()), string);
		sharedPrefencesEditor.apply();
	}



	public static Boolean isPushNotificationEnabled(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences("PushNotificationEnabled", Context.MODE_PRIVATE);
		return prefs.getBoolean("PushNotificationEnabled",true);

	}
	public static boolean isNetworkAvailable(Context mcontext) {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) mcontext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	public static void showAlertFeedNotification(Context context,String alertData){
		new AlertDialog.Builder(context)
				.setTitle(context.getResources().getString(R.string.app_name))
				.setMessage(alertData)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// continue with delete
					}
				})
				.show();
	}

	public static void showRatingDialog(final Context context){
		SharedPreferences prefs1 = context.getSharedPreferences("appOpenCount", Context.MODE_PRIVATE);
		if (prefs1.getInt("appOpenCount",0)>20) {
			SharedPreferences prefs = context.getSharedPreferences("isRatingDialogShowed", Context.MODE_PRIVATE);
			if (!prefs.getBoolean("isRatingDialogShowed", false)) {
				SharedPreferences.Editor editor = context.getSharedPreferences("isRatingDialogShowed", Context.MODE_PRIVATE).edit();
				editor.putBoolean("isRatingDialogShowed", true);
				editor.commit();
				new AlertDialog.Builder(context)
						.setTitle("Rate Us")
						.setMessage("If you enjoy using " + context.getResources().getString(R.string.app_name) + ", please take a moment to rate it. Thanks for you support")
						.setPositiveButton("Rate it!", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
								Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

								goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
										Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
										Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
								try {
									context.startActivity(goToMarket);
								} catch (ActivityNotFoundException e) {
									context.startActivity(new Intent(Intent.ACTION_VIEW,
											Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
								}
							}
						})
						.setCancelable(false)
						.setNegativeButton("No thanks", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {

							}
						})
						.show();
			}
		}
	}

}