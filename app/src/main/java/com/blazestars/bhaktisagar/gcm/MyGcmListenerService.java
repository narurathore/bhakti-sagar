
/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blazestars.bhaktisagar.gcm;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.blazestars.bhaktisagar.CommonActivities.DailyPanchangActivity;
import com.blazestars.bhaktisagar.CommonActivities.MainActivity;
import com.blazestars.bhaktisagar.CommonActivities.MainHomePageActivity;
import com.blazestars.bhaktisagar.CommonActivities.StoryDetailActivity;
import com.blazestars.bhaktisagar.Helper.Helper;
import com.blazestars.bhaktisagar.R;
import com.blazestars.bhaktisagar.entity.NameConstant;
import com.blazestars.bhaktisagar.entity.Story;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.nio.charset.Charset;

public class MyGcmListenerService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private String notification_type;
    private int _id;
    private String _title;
    private String _message;

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            // TODO(developer): Handle FCM messages here.
            // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
            Log.d(TAG, "From: " + remoteMessage.getFrom());

            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.d(TAG, "Message data payload: " + remoteMessage.getData());
                _title = remoteMessage.getData().get("title");
                _message=remoteMessage.getData().get("alert");
            }

            _id = (int) (long) System.currentTimeMillis();
            final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
            final Charset UTF_8 = Charset.forName("UTF-8");
            byte ptext[] = _message.getBytes(ISO_8859_1);
            _message = new String(ptext, UTF_8);
            //if (Helper.isPushNotificationEnabled(this)) {
                handleNofificationNavigation(this, remoteMessage);
            //}
        } catch (Exception e) {
            Log.d(TAG, "gcm error: " + e.getMessage());
            e.printStackTrace();
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */

        // [END_EXCLUDE]
    }
    // [END receive_message]



    private void handleNofificationNavigation(Context context,RemoteMessage remoteMessage) {
        try {

            notification_type = remoteMessage.getData().get("notification_type");
            if(!"".equalsIgnoreCase(notification_type)) {
                if (notification_type.equalsIgnoreCase("OPEN_APP")) {
                    openApp(context);
                }else if ("post_promotion".equalsIgnoreCase(notification_type)){
                    String post_id = remoteMessage.getData().get("post_id");
                    if (!"".equalsIgnoreCase(post_id))
                        gotoPost(context,post_id);

                }else if ("category_promotion".equalsIgnoreCase(notification_type)){
                    String category_id = remoteMessage.getData().get("category_id");
                    String categoryName = remoteMessage.getData().get("category_name");
                    if (!"".equalsIgnoreCase(category_id) && !"".equalsIgnoreCase(categoryName))
                        openCategory(context,category_id,categoryName);

                }else if ("general".equalsIgnoreCase(notification_type)){
                    openApp(context);
                }else if ("open_panchang".equalsIgnoreCase(notification_type)){
                    openPanchang(context);
                }else {
                    openApp(context);
                }
            }else {
                openApp(context);
            }
        } catch (Exception e) {

        }
    }

    private void push(Context _context,PendingIntent contentIntent) {
        int notifyID = _id;
        Bitmap notificationLargeIconBitmap = BitmapFactory.decodeResource(
                _context.getResources(),
                R.drawable.notification_icon);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(_context)
                        .setContentTitle(_title)
                        .setTicker(null)
                        .setContentText(_message)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setLargeIcon(notificationLargeIconBitmap)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(_title));
        mBuilder.setTicker(null);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setAutoCancel(true);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        NotificationManager mNotificationManager = (NotificationManager)
                _context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(contentIntent);
    }

    private void gotoPost(final Context context,String post_id){
        Intent intent = new Intent(context, StoryDetailActivity.class);
        intent.putExtra("notification", true);
        intent.putExtra("position", "0");
        Story story = new Story();
        story.postId = post_id;
        intent.putExtra("story", story);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, _id,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        push(context, contentIntent);
    }

    private void openCategory(final Context context,String category_id,String categoryName){
        Intent intent = new Intent(context, MainHomePageActivity.class);
        intent.putExtra("notification", true);
        intent.putExtra("category_id", category_id);
        intent.putExtra("category_name", categoryName);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        push(context, contentIntent);
    }

    private void openApp(Context context) {
        Intent intent;
        intent = new Intent(context, MainHomePageActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, _id,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        push(context, contentIntent);
    }

    private void openPanchang(Context context) {
        Intent intent;
        intent = new Intent(context, DailyPanchangActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, _id,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        push(context, contentIntent);
    }
}